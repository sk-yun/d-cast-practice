const approot = require('app-root-path');
const path = require('path');
const filename = path.basename(__filename);
const moment = require('moment');
const payloadModel = require('./payload');
const esServerService = require(`${approot}/middleware/es.server.service`);
const esClientService = require(`${approot}/middleware/es.client.service`);
const DBService = require(`${approot}/middleware/jdbc.service`);

// Index Name or Doc
const dbVenderIndex = 'dcast_db_vendor';
const dbConfigIndex = 'dcast_db_config';
const esConfigIndex = 'dcast_es_config';
const docType = '_doc';

/*
  서버 Elasticsaerch 관련 err는 throw err로 알리고,
  그 외 db및 client es 관련 connection err는 화면 출력용으로 return함.
*/
module.exports = {
  esConnTest: async (req) => {
    try {
      req.on('aborted', () => {
        return {
          status: false,
          message: 'clientCancelledRequest'
        };
      });

      const paramSet = req.query;
      const url = paramSet.url || paramSet.host + ':' + paramSet.port;

      let esConTest = await esClientService.initialize(url);
      const client = esConTest.client;

      if (esConTest.status && !paramSet.url) {
        await client.cat.health({
          format: 'json'
        }).then((r) => {
          esConTest.cluster = r[0].cluster;
        });
    
        await client.cat.nodes({
          format: 'json'
        }).then((r) => {
          esConTest.node = r[0].name;
        });
      }
      delete esConTest.client;

      return esConTest;
    } catch (err) {
      console.log(err);
      err.filename = err.filename || filename;
      throw err;
    }
  },
  createESConfig: async (config) => {
    try {
      let payload = {
        ...config,
        create_date: moment().format('YYYY-MM-DD HH:mm:ss')
      };
      let result = esServerService.addDocument(esConfigIndex, docType, payload);
      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  },
  esConfigList: async (config) => {
    try {
      let payload = payloadModel.ConfigPayload;
      payload.size = config.size;
      payload.from = config.from;
      payload.query.bool.must[0].match['owner.keyword'] = config.owner;

      let data = await esServerService.search(esConfigIndex, docType, payload);
      let total = data.hits.total;
      let dataSet = data.hits.hits;

      let result = [];
      for (let i = 0; i < dataSet.length; i++) {
        result.push({
          _id: dataSet[i]._id,
          ...dataSet[i]._source
        });
      }
      return { total, result };
    } catch (err) {
      console.log(err);
      err.filename = err.filename || filename;
      throw err;
    }
  },
  deleteESConfig: async (id) => {
    try {
      let result = await esServerService.deleteDocument(esConfigIndex, id, docType);
      result = result.result;
      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  },
  updateESConfig: async (config) => {
    try {
      let _id = config.id;
      delete config.id;

      let payload = {
        doc: {
          ...config,
          update_date: moment().format('YYYY-MM-DD HH:mm:ss')
        }
      };
      let result = esServerService.update(esConfigIndex, _id, docType, payload);
      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  },
  vendorList: async () => {
    try {
      let data = await esServerService.search(dbVenderIndex, docType, '');

      let result = [];
      for (let val of data.hits.hits) {
        val._source._id = val._id;
        result.push(val._source);
      }

      return result;
    } catch (err) {
      console.log(err);
      err.filename = err.filename || filename;
      throw err;
    }
  },
  dbConnTest: async (req, config) => {
    try {
      req.on('aborted', () => {
        return {
          status: false,
          message: 'clientCancelledRequest'
        };
      });

      let Pool = new DBService();
      Pool.setUp(config);
      let result = await Pool.initialize();
      await Pool.close();
      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  },
  createDBConfig: async (config) => {
    try {
      let payload = {
        ...config,
        create_date: moment().format('YYYY-MM-DD HH:mm:ss')
      };
      let result = esServerService.addDocument(dbConfigIndex, docType, payload);
      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  },
  updateDBConfig: async (config) => {
    try {
      let _id = config.id;
      delete config.id;

      let payload = {
        doc: {
          ...config,
          update_date: moment().format('YYYY-MM-DD HH:mm:ss')
        }
      };
      let result = esServerService.update(dbConfigIndex, _id, docType, payload);
      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  },
  dbConfigList: async (config) => {
    try {
      let payload = payloadModel.ConfigPayload;
      payload.size = config.size;
      payload.from = config.from;
      payload.query.bool.must[0].match['owner.keyword'] = config.owner;

      let data = await esServerService.search(dbConfigIndex, docType, payload);
      let total = data.hits.total;
      let dataSet = data.hits.hits;

      let result = [];
      for (let i = 0; i < dataSet.length; i++) {
        result.push({
          _id: dataSet[i]._id,
          ...dataSet[i]._source
        });
      }
      return { total, result };
    } catch (err) {
      console.log(err);
      err.filename = err.filename || filename;
      throw err;
    }
  },
  deleteDBConfig: async (id) => {
    try {
      let result = await esServerService.deleteDocument(dbConfigIndex, id, docType);
      result = result.result;
      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  }
};

const approot = require('app-root-path');
const path = require('path');
const filename = path.basename(__filename);
const moment = require('moment');
const payloadModel = require('./payload');
const esServerService = require(`${approot}/middleware/es.server.service`);
const esClientService = require(`${approot}/middleware/es.client.service`);
const DBService = require(`${approot}/middleware/jdbc.service`);

// Index Name or Doc
const dbConfigIndex = 'dcast_db_config';
const esConfigIndex = 'dcast_es_config';
const docType = '_doc';


module.exports = {
  executeQuery: async (req, params) => {
    try {
      req.on('aborted', () => {
        return {
          status: false,
          message: 'clientCancelledRequest'
        };
      });

      let config = params.db_config;
      let query = params.query;

      let Pool = new DBService();
      Pool.setUp(config);
      await Pool.initialize();
      await Pool.createStatement();
      await Pool.setMaxRows(100);
      
      let result = await Pool.executeQuery(query);
      await Pool.close();

      return result;
    } catch (err) {
      return {
        status: false,
        message: err
      };
    }
  }
};

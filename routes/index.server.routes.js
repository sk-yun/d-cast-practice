const account = require('./controllers/account');
const connection = require('./controllers/connection');
const index = require('./controllers/index');

module.exports = function (app) {
  app.use('/api/account', account);
  app.use('/api/conn', connection);
  app.use('/api/index', index);
};

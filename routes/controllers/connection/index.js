const express = require('express');
const router = express.Router();
const controller = require('./connection.controller');

router.get('/', controller.api);
router.post('/', controller.api);
router.get('/vendorList', controller.vendorList);
router.post('/vendorList', controller.vendorList);
router.post('/dbConnTest', controller.dbConnTest);
router.post('/createDBConfig', controller.createDBConfig);
router.post('/dbConfigList', controller.dbConfigList);
router.post('/deleteDBConfig', controller.deleteDBConfig);
router.post('/esConnTest', controller.esConnTest);
router.post('/createESConfig', controller.createESConfig);
router.post('/esConfigList', controller.esConfigList);
router.post('/deleteESConfig', controller.deleteESConfig);
router.post('/updateDBConfig', controller.updateDBConfig);
router.post('/updateESConfig', controller.updateESConfig);

module.exports = router;

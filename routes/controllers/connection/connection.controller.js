/**
 * API CONNECTION
 * */
const approot = require('app-root-path');
const path = require('path');
const deepcopy = require('deepcopy');
const fileName = path.basename(__filename);
const Util = require(`${approot}/util/util.js`);
const connection = require(`${approot}/routes/model/connection.model`);
const moment = require('moment');
require(`${approot}/util/compareJSON`);


module.exports = {
  api: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);

      const paramSet = req.query;
      // logic
      
      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {
      };
      // Set Response
      payload.response = {
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  vendorList: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);

      const paramSet = req.query;
      // logic
      let venderList = await connection.vendorList();

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {};

      // Set Response
      payload.response = {
        conn_vendor_data: venderList
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  dbConnTest: async (req, res) => {
    let cancelRequest = false;
    
    try {
      req.on('aborted', () => {
        try {
          cancelRequest = true;
          // console.log(childProcess);
          // console.log(process);
          // process.kill(process.pid, 'SIGINT');
          return false;
        } catch (err) {
          console.log('Kill Error:::', err);
          return err;
        }
      });
      // let checkCancelledRequest = setInterval(() => {
      //   if (cancelRequest) {
      //     console.log('in', cancelRequest);
      //     clearInterval(checkCancelledRequest);
      //     throw new Error('clientCancelledRequest');
      //   }
      // }, 1);

      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      // console.log('before::', jobs);
      
      const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      const config = deepcopy(req.query);
      const jobTime = moment(new Date()).valueOf();
      // default
      config.minpoolsize = 1;
      config.maxpoolsize = 3;

      // logic
      const paramSet = req.query;
      delete paramSet.id; // job 구분 용도로 사용된 id값 제거
      // let connTest = await connection.dbConnTest(paramSet);
      let connTest = await connection.dbConnTest(req, paramSet);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_database_connection: connTest
      };
      req.query.body = payload;
      // console.log('after::', jobs);
      // clearInterval(checkCancelledRequest);
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      console.log('catch', err);
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  createDBConfig: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;

      // logic
      let result = await connection.createDBConfig(config);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_create_dbconfig: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  dbConfigList: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;
      // logic
      let { total, result } = await connection.dbConfigList(config);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_dbconfig_total: total,
        conn_dbconfig_data: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  deleteDBConfig: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;
      const id = config._id;
      // logic
      let result = await connection.deleteDBConfig(id);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_database_delete: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  esConnTest: async (req, res) => {
    let cancelRequest = false;
    
    try {
      req.on('aborted', () => {
        try {
          cancelRequest = true;
          // console.log(childProcess);
          // console.log(process);
          // process.kill(process.pid, 'SIGINT');
          return false;
        } catch (err) {
          console.log('Kill Error:::', err);
          return err;
        }
      });
      // let checkCancelledRequest = setInterval(() => {
      //   if (cancelRequest) {
      //     console.log('in', cancelRequest);
      //     clearInterval(checkCancelledRequest);
      //     throw new Error('clientCancelledRequest');
      //   }
      // }, 1);

      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      // console.log('before::', jobs);
      
      // logic
      
      // let connTest = await connection.dbConnTest(paramSet);
      let connTest = await connection.esConnTest(req);
      console.log(connTest);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_elasticsearch_connection: connTest
      };
      req.query.body = payload;
      // console.log('after::', jobs);
      // clearInterval(checkCancelledRequest);
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      console.log('catch', err);
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  createESConfig: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;

      // logic
      let result = await connection.createESConfig(config);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_create_esconfig: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  esConfigList: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;
      // logic
      let { total, result } = await connection.esConfigList(config);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_esconfig_total: total,
        conn_esconfig_data: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  deleteESConfig: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;
      const id = config._id;
      // logic
      let result = await connection.deleteESConfig(id);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_elasticsearch_delete: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  updateDBConfig: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;

      // logic
      let result = await connection.updateDBConfig(config);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_update_dbconfig: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  updateESConfig: async (req, res) => {
    try {
      req.paramStatus = 'Api Connection';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);
      
      const config = req.query;

      // logic
      let result = await connection.updateESConfig(config);

      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {

      };
      // Set Response
      payload.response = {
        conn_update_esconfig: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  }
};


/*
  // ### Jobs 관리 로직 ###
  let jobList = new Map();
  let jobSet = [];
  let job = {};

  // 해당 ip에 같은 url의 미완료된 connection test 작업이 있는지 확인.
  if (jobs.get(ip) && jobs.get(ip).get(config.url)) {
    // 해당 작업이 존재하는지 확인.
    jobList = jobs.get(ip);
    jobSet = jobList.get(config.url);
    // console.log('jobSet:::', jobSet);
    // console.log('jobSet:::', jobSet.length > 0);
    // console.log('jobSet:::', jobSet && jobSet.length > 0);

    // 작업 배열이 존재하고 배열 안에 값이 존재하면 같은 config를 가지고 있는지 확인
    if (jobSet && jobSet.length > 0) {
      for (let i = 0; i < jobSet.length; i++) {
        let jobConfig = jobSet[i].config;
        // 두 config JSON 객체가 같은 값을 가지고있는지 확인.
        // console.log('jobConfig:::', jobConfig);
        // console.log('config:::', config);

        console.log(Object.equals(jobConfig, config));
        if (Object.equals(jobConfig, config)) {
          // 같은 값을 가진 config JSON 객체가 존재하면
          // 지금 작업을 실시할 수 없음을 return.

          // throw new Error('비정상적으로 종료된 Connection Test가 존재합니다. 잠시 후 다시 시도해 주시기 바랍니다.');
          throw new Error('There is \'Connection Test\' that ended abnormally. Please try again later.');
        }
      }
    }
  } else if (jobs.get(ip)) {
    jobList = jobs.get(ip);
  }
  
  job = {
    config: config,
    jobTime: jobTime
  };

  jobSet.push(job);
  jobList.set(config.url, jobSet);
  jobs.set(ip, jobList);
  // ### Jobs 관리 로직 End ###
*/
/*
  // ### Jobs 관리 로직(job 제거) ###
  let updateJobs = jobs.get(ip).get(config.url);
  for (let i = 0; i < updateJobs.length; i++) {
    let jobConfig = updateJobs[i].config;
    if (Object.equals(jobConfig, config)) {
      // 해당 요소 제거
      updateJobs.splice(i, 1);
      if (updateJobs.length > 0) {
        // 해당 url에 관련된 connection 정보가 있으면 updateJosb를 set한다.
        // 해당 ip에 잡혀있는 job이 1개보다 많으면 updateJosb를 set한다.
        let beforeJobs = jobs.get(ip);
        beforeJobs.set(jobConfig.url, updateJobs);
        jobs.set(ip, beforeJobs);
      } else if (updateJobs.length === 0 && jobs.get(ip).size > 1) {
        // 해당 url에 관련된 connection 정보가 없고,
        // 해당 ip에 잡혀있는 job이 1개보다 많으면 해당 url 관련 map 요소만 제거한다.
        let beforeJobs = jobs.get(ip);
        beforeJobs.delete(jobConfig.url);
        jobs.set(ip, beforeJobs);
      } else if (updateJobs.length === 0 && jobs.get(ip).size <= 1) {
        // 해당 url에 관련된 connection 정보가 없고,
        // 해당 ip에 잡혀있는 job이 1개면 해당 ip 관련 map 요소만 제거한다.
        jobs.delete(ip);
      }
    }
  }
  // ### Jobs 관리 로직(job 제거) End ###
*/

const express = require('express');
const router = express.Router();
const controller = require('./index.controller');

router.get('/', controller.api);
router.post('/', controller.api);
router.post('/configList', controller.configList);
router.post('/executeQuery', controller.executeQuery);

module.exports = router;

/**
 * API INDEX
 * */
const approot = require('app-root-path');
const path = require('path');
const deepcopy = require('deepcopy');
const fileName = path.basename(__filename);
const Util = require(`${approot}/util/util.js`);
const connection = require(`${approot}/routes/model/connection.model`);
const index = require(`${approot}/routes/model/index.model`);
const moment = require('moment');

module.exports = {
  api: async (req, res) => {
    try {
      req.paramStatus = 'Api Index';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);

      const paramSet = req.query;
      // logic
      
      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {
      };
      // Set Response
      payload.response = {
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  configList: async (req, res) => {
    try {
      req.paramStatus = 'Api Index';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);

      const config = req.query;
      config.size = 100; // 일단, 최대 100건으로 한정
      config.from = 0;
      
      // logic
      let dbResult = await connection.dbConfigList(config);
      let esResult = await connection.esConfigList(config);
      
      let result = {
        dbConfigList: dbResult.result,
        esConfigList: esResult.result
      };
      
      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {
      };
      // Set Response
      payload.response = {
        index_config_data: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
  executeQuery: async (req, res) => {
    try {
      req.paramStatus = 'Api Index';
      const paramErr = Util.param_check(req, res, fileName, []);
      if (paramErr.status) return res.status(400).send(paramErr.errMsg);

      const paramSet = req.query;
      
      // logic
      let result = await index.executeQuery(req, paramSet);
      
      const payload = { request: {}, response: {} };

      // Set Request
      payload.request = {
      };
      // Set Response
      payload.response = {
        index_execute_data: result
      };

      req.query.body = payload;
      return Util.sendResult(req, res, 'success');
    } catch (err) {
      err.filename = err.filename || fileName;
      Util.errorlog(err, err.filename);
      return Util.sendResult(req, res, 'error', err, err.filename);
    }
  },
};

import React, { useState, createContext } from 'react';
const ConfigListContext = createContext();
const ConfigListProvider = (props) => {

  const [dbList, setDBList] = useState([]);
  /*
      # Data Form
    {
       Connection: ƒ ConnTest() {},
       Create_Date: "2020-03-16 00:00:00",
       Delete: <DeleteButton />,
       Session: "sessionName",
       State: "Waiting",
       Url: "jdbc:postgresql://localhost:5432/test"
    },
    { ... }

  */

  const [esList, setESList] = useState([]);

  return (
    <ConfigListContext.Provider value={[dbList, setDBList, esList, setESList]}>
      {props.children}
    </ConfigListContext.Provider>
  );
};

export { ConfigListProvider, ConfigListContext };

import React, { useState, createContext } from 'react';
const JobListContext = createContext();
const JobListProvider = (props) => {

  const [dbList, setDBList] = useState([]);
  const [dbCount, setDBCount] = useState(0);
  const [esList, setESList] = useState([]);
  const [shouldBlock, setShoudlBlock] = useState(false);

  return (
    <JobListContext.Provider value={[dbList, setDBList, dbCount, setDBCount, esList, setESList]}>
      {props.children}
    </JobListContext.Provider>
  );
};

export { JobListProvider, JobListContext };

import React, { useState, createContext } from 'react';
const UserInfoContext = createContext();
const UserInfoProvider = (props) => {

  const [userInfo, setUserInfo] = useState({
    isLogged: false,
    userId: '',
    userName: ''
  });

  return (
    <UserInfoContext.Provider value={[userInfo, setUserInfo]}>
      {props.children}
    </UserInfoContext.Provider>
  );
};

export { UserInfoProvider, UserInfoContext };

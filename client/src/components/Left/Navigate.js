import React, { useContext } from 'react';
import { NavLink, Link } from 'react-router-dom';
import { CollapseContext } from 'contexts/CollapseContext';
import './Left.scss';

const navList = [
  { name: 'connection', explain: 'Connection', url: '/conn' },
  { name: 'index', explain: 'Index', url: '/index' },
  { name: 'search', explain: 'Search', url: '/search' },
  { name: 'statistics', explain: 'Statistics', url: '/stat' },
  { name: 'service', explain: 'Service', url: '/service' },
  { name: 'schedule', explain: 'Schedule', url: '/schedule' },
  { name: 'setting', explain: 'Setting', url: '/setting' }
];

const CollapseButton = ({ active, onClick }) => {
  return (
    <div className={`colItem ${active ? 'active' : ''}`} onClick={onClick}>
      {active ? '접기' : '열기'}
    </div>
  );
};

const Navigate = () => {

  const [isCollapse, setCollapse] = useContext(CollapseContext);

  const onHandle = () => {
    if (isCollapse) setCollapse(false);
    else setCollapse(true);
  };
  // page[item.explain]

  return (
    <React.Fragment>
      {
        navList.map((item) => {
          return (
            <NavLink className="Link" to={`${item.url}`} key={item.name}>
              <ul className="navi" title={`${isCollapse ? '' : item.explain}`}>
                <li className={`icn ${item.name} left_menu`} />
                {isCollapse ? <li className="text">{item.explain}</li> : ''}
              </ul>
            </NavLink>
          );
        })
      }
      <CollapseButton active={isCollapse === true} onClick={() => onHandle()} />
    </React.Fragment>
  );
};

export default Navigate;

import React, { useContext } from 'react';
import { withRouter, Route, Switch, BrowserRouter as Router, Redirect } from 'react-router-dom';
import { CollapseContext } from 'contexts/CollapseContext';
import './Content.scss';
import Left from 'components/Left';
import NotFound from 'components/Common/NotFound';
import Connection from 'components/Contents/Connection';
import Index from 'components/Contents/IndexSet';

const index = () => {
  return (
    <div className="v_content">
      <Route>
        <Redirect to={{ pathname: '/conn' }} />
      </Route>
    </div>
  );
};

const Content = () => {
  const [isCollapse] = useContext(CollapseContext);

  return (
    <div className="v_content">
      <Router>
        <div className="vc_middle">
          <Left />
          <div className={`vc_content ${isCollapse ? 'collpase' : ''}`}>
            <Switch>
              <Route exact path="/" component={Connection} />

              <Route exact path="/conn" component={Connection} />
              <Route path="/conn/:section" component={Connection} />

              <Route exact path="/index" component={Index} />
              <Route path="/index/:section" component={Index} />

              {/* <Route path="/Increase" component={Increase} />
              <Route path="/Relation" component={Relation} />
              <Route path="/Contents" component={Contents} /> */}

              <Route path="/NotFound" component={NotFound} />
              <Redirect from="*" exact to="/NotFound" />
            </Switch>
          </div>
        </div>
      </Router>

    </div>
  );
};

export default withRouter(Content);

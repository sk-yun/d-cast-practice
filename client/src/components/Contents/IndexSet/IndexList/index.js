import React, { useState, useContext, useEffect, useCallback } from 'react';
import Axios from 'axios';
import sha512 from 'crypto-js/sha512';
import AddButton from 'common/Button/AddButton';
import DeleteButton from 'components/Contents/Connection/Common/DeleteButton';
import BorderButton from 'common/Button/BorderButton';
import Table from 'common/Table';
import CustomRecord from 'components/Contents/Connection/Common/CustomRecord';
import Paging from 'common/Paging';
import Alert from 'common/Modal/ModalAlert';
import Config from 'config';

// context
import { UserInfoContext } from 'contexts/UserInfoContext';

// Axios cancel request token
const CancelToken = Axios.CancelToken;

const DatabaseList = () => {
  const [userInfo] = useContext(UserInfoContext);
  const [indexList, setIndexList] = useState([]);
  const [tableList, setTableList] = useState([]);
  const [total, setTotal] = useState(0);

  // alertModal State
  const [alertModal, setAlertModal] = useState({
    show: false,
    title: '',
    content: ''
  });

  const toggleAlert = () => {
    setAlertModal({
      show: false,
      title: '',
      content: ''
    });
  };

  const getIndexList = async () => {
    console.log('getIndexList:::');
  };


  // Table header Set
  const headerSet = [
    { field: 'Name', type: 'text' },
    { field: '수집원', type: 'text', style: { width: '150px' } },
    { field: 'ELS List', type: 'text' },
    { field: '수집', type: 'function' },
    { field: 'Create Date', type: 'text' },
    { field: 'Delete', type: 'function', style: { minWidth: '50px' } },
  ];

  return (
    <React.Fragment>
      <div className="ct_layout">
        <div className="ct_header">
          Index 설정 목록
        </div>
        <div className="ct_title">
          Index List
        </div>
        <div className="ct_box">
          <AddButton url="/index/indexconfig" text="Add Index" />
          <Table headerSet={headerSet} data={tableList} CustomRecord={CustomRecord} />
          <div className="ct_box_footer" style={{ width: '1102px' }}>
            <Paging
              onClick={getIndexList}
              totalCount={total}
              listCount={5}
              displayCount={5}
              current={1}
            />
          </div>
        </div>
      </div>
      <Alert
        view={alertModal.show}
        title={alertModal.title}
        content={alertModal.content}
        hide={toggleAlert}
      />
    </React.Fragment>
  );
};

export default DatabaseList;

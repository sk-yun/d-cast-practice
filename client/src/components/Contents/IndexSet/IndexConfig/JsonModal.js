import React from 'react';
import 'common/Modal/Modal.scss';

const ModalContents = (props) => {
  const index = props.data.index;
  const json = props.items.json;

  let selectJson = [];
  if (index === -1) {
    // index가 -1인 경우, result json 전체를 출력
    selectJson = json;
  } else {
    // index가 -1이 아닌 경우, result json의 한 index만 출력
    selectJson = json[index];
  }

  return (
    <React.Fragment>
      <div className="modal_content" style={{ height: '550px', overflow: 'auto', padding: '20px 10px', display: 'inline-block' }}>
        <div className="box_div">
          <div className="box_layout noshadow">
            <div className="_content fx_h_380">
              <div className="grid_box">
                <pre id="json_pre">
                  {JSON.stringify(selectJson, null, 2)}
                </pre>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default ModalContents;

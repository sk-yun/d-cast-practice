import React from 'react';
import './Table.css';

const Thead = (props) => {
  const elements = [];

  if (props.columns.length && props.columns.length !== 0) {
    for (let i = 0; i < props.columns.length; i++) {
      elements.push(<td key={props.columns[i]}>{props.columns[i]}</td>);
    }
  } else {
    for (let i = 0; i < 7; i++) {
      elements.push(<td key={i} style={{ width: '200px', backgroundColor: '#eee' }}> - </td>);
    }
  }

  return (
    <React.Fragment>
      <thead>
        <tr>
          {elements}
        </tr>
      </thead>
    </React.Fragment>
  );
};

const Tbody = (props) => {
  if (!props.data || props.data.length === 0) return false;

  let count = 0;
  return (
    <React.Fragment>
      <tbody>
        {
            props.data.map((rows) => {
              return (
                <Record
                  key={count}
                  columns={props.columns}
                  rows={rows}
                  index={count++}
                  openJsonModal={props.openJsonModal}
                />
              );
            })
        }
      </tbody>
    </React.Fragment>
  );
};

const Record = (props) => {
  if (!props.rows || props.rows.length === 0) return false;
  const index = props.index;
  const openJsonModal = props.openJsonModal;

  let count = 0;
  return (
    <tr onDoubleClick={() => openJsonModal(index)}>
      {
        props.columns.map((cols) => {
          return (
            <td key={count++}>
              {
                // decimal과 같은 Object로 넘어오는 데이터가 있을 수 있을까봐
                // 추가한 코드. decimal은 처리 해놓음.
                typeof props.rows[cols] === 'object'
                  ? 'Object : ' + JSON.stringify(props.rows[cols])
                  : props.rows[cols]
              }
            </td>
          );
        })
      }
    </tr>
  );
  
};

const DataTable = (props) => {
  return (
    <table className="_resTable">
      <Thead columns={props.columns} />
      <Tbody columns={props.columns} data={props.data} openJsonModal={props.openJsonModal} />
    </table>
  );
};

export default DataTable;

/* eslint-disable jsx-a11y/label-has-for */
import React, { useState, useEffect } from 'react';
import Select from 'common/Select/Select';
import BorderButton from 'common/Button/BorderButton';
import Modal from 'common/Modal/ModalCover';
import ModalContent from './SubQueryModal';

const ColName = (props) => {
  return (
    <div>
      <div className="tc_text">
        {props.colName}
      </div>
    </div>
  );
};

const FieldName = (props) => {
  return (
    <div>
      <input
        type="text"
        value={props.fieldName}
        onChange={props.setValue}
      />
    </div>
  );
};

const UseMapping = (props) => {

  return (
    <div>
      <input
        type="checkbox"
        id={`ckeckbox_${props.index}`}
        defaultChecked={props.useMapping}
        onChange={e => props.setValue(e.target.checked)}
      />
      <label htmlFor={`ckeckbox_${props.index}`}>{}</label>
    </div>
  );
};

const Type = (props) => {
  return (
    <div>
      {
        props.useMapping
          ? (
            <Select
              default={props.type}
              list={props.typeList}
              setValue={props.setValue}
              style={{ width: '100px' }}
            />
          )
          : null
      }
    </div>
  );
};

const Analyzer = (props) => {
  return (
    <div>
      {
        props.useMapping
          ? (
            <Select
              default={props.analyzer}
              list={props.analyzerList}
              setValue={props.setValue}
              style={{ width: '100px' }}
            />
          )
          : null
      }
    </div>
  );
};

const MultiField = (props) => {
  return (
    <div>
      <div className="tc_text">
        <BorderButton
          addClass="multiField_set"
                // onHandle={e => Create(e)}
          name="설 정"
          style={{ width: '120px', fontWeight: '600', fontSize: '13px', borderColor: 'cornflowerblue' }}
        />
      </div>
    </div>
  );
};

const SubQuery = (props) => {
  return (
    <div>
      <div className="tc_text">
        <BorderButton
          addClass="subquery_set"
                // onHandle={e => Create(e)}
          name="SET"
          onClick={props.viewModal}
          style={{ width: '120px', fontWeight: '600', fontSize: '13px', borderColor: 'cornflowerblue' }}
        />
      </div>
    </div>
  );
};

// Table의 한 개 row
const Record = (props) => {

  const colName = props.colName;
  const index = props.index;

  // 나중에는 저장되어있는 리스트를 불러와서 적용하도록 수정하는 것이 맞을 듯.
  const typeList = [
    'keyword', 'text', 'integer', 'long', 'short', 'byte', 'double', 'double', 'float',
    'boolean', 'date', 'binary', 'object', 'Geo-Point', 'IP', 'Nested', 'Custom Mode'
  ];
  const analyzerList = [
    'standard', 'keword', 'kobrick', 'ngram', 'ngram_v2'
  ];

  // 한 개 Row가 가지는 데이터 형식의 Template
  const rowDataTemplate = {
    fieldName: colName,
    useMapping: true,
    type: typeList[0],
    analyzer: analyzerList[0],
    multiField: {
      set: false
    },
    subQuery: {
      set: false
    }
  };

  const [mappingData, setMappingData] = useState(props.state);
  const [rowData, setRowData] = useState({
    colName: colName,
    field: rowDataTemplate
  });
  const [subRowData, setSubRowData] = useState([]);

  const modifiedSubRowData = () => {
    // setSubRowData(
    //   subRowData.map(item => item.index === index
    //     ? state
    //     : item)
    // );
  };
  
  // const modifiedMappingData = () => {
  //   setMappingData(
  //     mappingData.map(item => item.index === index
  //       ? state
  //       : item)
  //   );
  // };

  const setSubQuery = () => {
    setSubRowData(subRowData.push(rowDataTemplate));
  };


  // Modal State
  const [isModal, setIsModal] = useState({
    view: false,
    data: {}
  });

  // close modal
  const toggleModal = () => {
    setIsModal({ ...isModal,
      view: !isModal.view,
      data: {}
    });
  };

  const viewModal = async () => {
    setIsModal({
      ...isModal,
      view: !isModal.view
    });
  };
  
  useEffect(() => {
    // modifiedMappingData();
  }, [rowData]);
  
  return (
    <tr key={index}>
      <td className="mt_colName" disabled>
        <div className="table_cell">
          <ColName colName={rowData.colName} />
          {
            subRowData.length > 0
              ? subRowData.map(() => (<ColName colName="" />))
              : null
          }
        </div>
      </td>
      <td className="mt_fieldName">
        <div className="table_cell">
          <FieldName
            fieldName={rowData.field?.fieldName}
            setValue={(e) => {
              setRowData({
                ...rowData,
                field: {
                  ...rowData.field,
                  fieldName: e.value
                }
              });
            }}
          />
          {
            subRowData.length > 0
              ? subRowData.map(item => (
                <FieldName
                  fieldName={item.fieldName}
                />
              ))
              : null
          }
        </div>
      </td>
      <td className="mt_mapping">
        <div className="table_cell">
          <UseMapping
            index={index}
            useMapping={rowData.field?.useMapping}
            setValue={(e) => {
              setRowData({
                ...rowData,
                field: {
                  ...rowData.field,
                  useMapping: e
                }
              });
            }}
          />
          {
            subRowData.length > 0
              ? subRowData.map(item => (<></>))
              : null
          }
        </div>
      </td>
      <td className="mt_type">
        <div className="table_cell">
          <Type
            useMapping={rowData.field.useMapping}
            type={rowData.field?.type}
            typeList={typeList}
            setValue={(e) => {
              setRowData({
                ...rowData,
                field: {
                  ...rowData.field,
                  type: e.value
                }
              });
            }}
          />
          {
            subRowData.length > 0
              ? subRowData.map(item => (
                <Type
                  useMapping={item.field.useMapping}
                  type={item.field?.type}
                  typeList={typeList}
                />
              ))
              : null
          }
        </div>
      </td>
      <td className="mt_analyzer">
        <div className="table_cell">
          <Analyzer
            useMapping={rowData.field.useMapping}
            analyzer={rowData.field.analyzer}
            analyzerList={analyzerList}
            setValue={e => setRowData({
              ...rowData,
              field: {
                ...rowData.field,
                analyzer: e.value
              }
            })}
          />
          {
            subRowData.length > 0
              ? subRowData.map(item => (
                <Analyzer
                  useMapping={item.field.useMapping}
                  analyzer={item.field.analyzer}
                  analyzerList={analyzerList}
                />
              ))
              : null
          }
        </div>
      </td>
      <td className="mt_multiField">
        <div className="table_cell">
          <MultiField />
          {
            subRowData.length > 0
              ? subRowData.map(item => (
                <MultiField />
              ))
              : null
          }
        </div>
      </td>
      <td className="mt_subquery">
        <div className="table_cell">
          <SubQuery
            viewModal={viewModal}
          />
          {
            subRowData.length > 0
              ? subRowData.map(item => (
                <SubQuery />
              ))
              : null
          }
        </div>
      </td>
      <Modal
        set={isModal}
        hide={toggleModal}
        contents={ModalContent}
        style={{ width: '550px', height: '465px' }}
      />
    </tr>
  );
};

export default Record;

import React, { useState } from 'react';
import Record from './Record';
import '../Table.css';

// Table의 Thead
const Thead = () => {
  const columns = ['DB 컬럼명', '필드명', '매핑 여부', '타입', 'Analyzer', '멀티 필드', 'Subquery'];

  return (
    <thead>
      <tr>
        {
          columns.map((col) => {
            return (<td key={col} style={{ width: '150px' }}>{col}</td>);
          })
        }
      </tr>
    </thead>
  );
};

// Table의 Tbody
const Tbody = (props) => {
  if (!props.columns || props.columns.length === 0) return false;

  let count = 0;
  
  return (
    <tbody>
      {
          props.columns.map((col) => {
            if (!col) col = 'null';
            
            const [mappingData, setMappingData] = props.state;
            // setMappingData(mappingData.concat({
            //   [col]: {
                
            //   }
            // }));

            return (
              <Record key={count} index={count++} colName={col} state={props.state} />
            );
          })
      }
    </tbody>
  );
};


const MappingTable = (props) => {
  const [mappingData, setMappingData] = useState([]);

  return (
    <table className="_mapTable">
      <Thead />
      <Tbody columns={props.columns} state={[mappingData, setMappingData]} />
    </table>
  );
};

export default MappingTable;

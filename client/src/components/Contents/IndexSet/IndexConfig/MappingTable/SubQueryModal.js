import React, { useState, useContext, useRef } from 'react';
import Input from 'common/Input/Input';
import Waiting from 'common/ConnResult/Waiting';
import ConnResult from 'common/ConnResult/ConnResult';
import BorderButton from 'common/Button/BorderButton';
import Config from 'config';
import 'common/Modal/Modal.scss';

const ModalContents = (props) => {

  return (
    <React.Fragment>
      <div className="modal_content _conn">
        <div className="box_div">
          <div className="box_layout noshadow">
            <div className="_conn">
              <div className="desc_div">
                <div className="desc_left">
                  <div className="conn_title">{state.config_name}</div>
                </div>
              </div>
            </div>

            <div className="_content fx_h_300">
              <div className="grid_box">
                <div className="rows">
                    test
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default ModalContents;

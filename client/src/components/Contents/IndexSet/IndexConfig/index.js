import React, { useState, useContext, useEffect, useRef, useCallback } from 'react';
import Axios from 'axios';
import sha512 from 'crypto-js/sha512';
import BorderButton from 'common/Button/BorderButton';
import Select from 'common/Select/Select';
import Input from 'common/Input/Input';
import Alert from 'common/Modal/ModalAlert';
import Tabs from 'common/Tabs';
import DataTable from './DataTable';
import MappingTable from './MappingTable';
import Modal from 'common/Modal/ModalCover';
import ModalContent from './JsonModal';
import Config from 'config';

// context
import { UserInfoContext } from 'contexts/UserInfoContext';

// Axios cancel request token
const CancelToken = Axios.CancelToken;

const DatabaseConfig = (props) => {
  const [userInfo] = useContext(UserInfoContext);
  const [selectDB, setSelectDB] = useState({});
  const [dbList, setDBList] = useState([]);
  const [selectES, setSelectES] = useState({});
  const [esList, setESList] = useState([]);
  
  // tabs
  const [currentCategory, setCurrentCategory] = useState('DATA');
  // DataTable
  const [jsonResult, setJsonResult] = useState([]);
  const [columns, setColums] = useState([]);
  const [resultData, setResultData] = useState([]);
  const queryArea = useRef('');
  // Mapping
  const [indexName, setIndexName] = useState('');
  const [aliasName, setAliasName] = useState('');
  
  let paramsName = '';
  if (props.location.state) {
    paramsName = props.location.state.name || '';
  }
  
  const [name, setName] = useState(paramsName);

  // Modal State
  const [isModal, setIsModal] = useState({
    view: false,
    data: {
      index: -1
    }
  });

  // close modal
  const toggleModal = () => {
    setIsModal({ ...isModal,
      view: !isModal.view,
      data: {}
    });
  };

  const viewModal = async (index) => {
    // result 결과를 받지 않은 상태에선 modal을 열지 못하게 함.
    if (index === -1 && resultData.length === 0) return false;

    setIsModal({
      ...isModal,
      view: !isModal.view,
      data: {
        index: index
      }
    });
  };

  // alertModal State
  const [alertModal, setAlertModal] = useState({
    show: false,
    title: '',
    content: ''
  });

  const toggleAlert = () => {
    setAlertModal({
      show: false,
      title: '',
      content: ''
    });
  };

  // textarea에 tab/enter키를 적용시키기 위한 코드
  const onPressTabKey = (e) => {
    let keyCode = e.keyCode || e.which;
    let target = e.target;

    // 텍스트 커서의 위치
    let start = target.selectionStart;
    let end = target.selectionEnd;

    // 입력을 감지하면 수정내용이 있는 것으로 판단하여
    // 페이지 이동 시, confirm창을 띄운다.
    if (!window.onbeforeunload) {
      window.onbeforeunload = function (e) {
        return '';
      };
    }

    if (keyCode === 9) { // tab
      target.value = target.value.substring(0, start)
      + '\t' + target.value.substring(end);

      // 텍스트 커서 위치 설정
      target.selectionStart = start + 1;
      target.selectionEnd = end + 1;
      e.preventDefault();
    } else if (keyCode === 13) { // enter
      let blankStart = target.value.substring(0, start).lastIndexOf('\n') + 1;
      if (blankStart === 0) return false; // 첫 줄 제외

      // lineValue의 첫 번째 값이 공백인 경우에만 처리
      if (target.value.charAt(blankStart) === '\t' || target.value.charAt(blankStart) === ' ') {
        let lineValue = target.value.substring(blankStart, end); // blank 채크를 위한 이전 라인의 텍스트
        let regExp = /[^\t\s]/g; // 탭과 공백을 제외한 나머지 문자들을 고르는 정규식
        let exec = regExp.exec(lineValue); // 공백 분리를 위해 사용

        let blank = lineValue.substring(0, exec ? exec.index : lineValue.length); // blank 추출
        target.value = target.value.substring(0, start) + '\n'
                        + blank + target.value.substring(end); // blank 삽입

        // 텍스트 커서 위치 설정
        target.selectionStart = start + blank.length + 1;
        target.selectionEnd = end + blank.length + 1;
        e.preventDefault();
      }
    }
  };

  // page 전환 block 함수
  useEffect(() => {
    const unblock = props.history.block(() => {
      // onbeforeunload가 null이 아니면
      // 작성으로 인해 함수가 등록된 상태이므로
      // 페이지를 벗어날 때, confirm창을 띄운다.
      // TODO: confirm은 추후 수정
      if (window.onbeforeunload && !window.confirm('작성중인 페이지에서 벗어나시겠습니까?')) {
        return false;
      }
      // 페이지를 벗어나면 onbeforeunload을 null로 초기화
      window.onbeforeunload = null;
      return true;
    });
    return () => {
      unblock();
    };
  }, [props.history]);

  // Get DB Config List
  const getCofigList = useCallback(async (count) => {
    // 초기화
    setDBList([]);
    setESList([]);

    // 새로고침이나 경로를 통해서 접속 시, userId 정보가 사라진다.
    // app.js에서 해당 정보를 session에서 가져오기 전에 호출되어
    // owner가 빈 값이 되는 문제가 있는데, 이를 해결하기 위한 코드.
    let owner = userInfo.userId;
    if (!owner || owner === '') {
      owner = window.sessionStorage.getItem(sha512('id'));
    }

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/index/configList`,
      method: 'post',
      data: {
        owner: owner
      }
    };
    try {
      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      let response = setData.data.data.response;
      let dataSet = response.index_config_data;
      setDBList(dataSet.dbConfigList);
      setESList(dataSet.esConfigList);
    } catch (e) {
      console.log('ERROR', e);
    }
  }, [userInfo.userId]);

  // TODO : 쿼리 수행 중 로딩창이 필요함.
  const Execute = async (count) => {
    if (!selectDB._id) {
      setAlertModal({
        show: true,
        title: '알림 메시지',
        content: 'Database를 선택해 주세요.'
      });
      return false;
    }

    let query = queryArea.current.value;
    if (query.trim() === '') {
      setAlertModal({
        show: true,
        title: '알림 메시지',
        content: 'Query를 작성하지 않았습니다.'
      });
      return false;
    }

    // 쿼리 적절성 판별하는 로직도 필요하려나?
    // SELECT 쿼리 외의 쿼리는 차단하는 로직이 필요하지 않을까?

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/index/executeQuery`,
      method: 'post',
      data: {
        db_config: {
          drivername: selectDB.drivername,
          url: selectDB.url,
          user: selectDB.user,
          password: selectDB.password
        },
        query: query
      }
    };
    try {
      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      let response = setData.data.data.response;
      let dataSet = response.index_execute_data;

      if (dataSet.result && dataSet.result.length !== 0) {
        setColums(Object.keys(dataSet.result[0]));
      }

      console.log(dataSet);
      console.log(dataSet.result);
      setResultData(dataSet.result);
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  // 한 줄 보기 이벤트를 위한 함수
  const onPreWrap = (e) => {
    let state = e.target.innerText;
    let pre = document.getElementById('json_pre');

    if (state.indexOf('ON') > 0) {
      e.target.innerText = '한 줄 보기 OFF';
      pre.style.whiteSpace = 'pre';
    } else if (state.indexOf('OFF') > 0) {
      e.target.innerText = '한 줄 보기 ON';
      pre.style.whiteSpace = 'pre-wrap';
    }
  };

  // Modal title text에 넣을 제목과 버튼
  const PreWrap = (
    <React.Fragment>
      Result - JSON
      <BorderButton
        onHandle={e => onPreWrap(e)}
        name="한 줄 보기 OFF"
        style={{
          width: '100px',
          height: '20px',
          fontWeight: '200',
          fontSize: '10px',
          borderColor: 'cornflowerblue',
          color: 'cornflowerblue',
          margin: '10px'
        }}
      />
    </React.Fragment>
  );

  // create Index Config
  const create = () => {

  };
  
  // 초기 DB List 받아오기.
  useEffect(() => {
    getCofigList(0);
  }, [getCofigList]);

  // Tabs Categories
  const categories = [
    {
      id: 1,
      name: 'DATA'
    },
    {
      id: 2,
      name: 'Mapping'
    }
  ];


  return (
    <React.Fragment>
      <div className="ct_layout">
        <div className="ct_header">
          Index 설정
        </div>
        <div className="ct_title">
          Index Setting
        </div>
        <div className="ct_box" style={{ height: 'auto' }}>
          <div className="box_conf_header">
            <Input name="Name" value={name} setValue={setName} style={{ minWidth: '600px' }} />
          </div>
          <div className="h50" />
          <div className="box_body">
            <div className="ct_subTitle">DB List</div>
            <div className="rows-mt-10">
              <Select column="config_name" defaultText="Select DataBase" list={dbList} setValue={setSelectDB} style={{ width: '450px' }} />
            </div>
            {/* 파일 첨부는 일단 생략 */}
            <div className="rows">
              <div className="query_area">
                <textarea ref={queryArea} onKeyDown={onPressTabKey} />
                <button type="button" onClick={Execute}>Execute</button>
              </div>
            </div>

            <div className="h100" />
            <div className="ct_subTitle">ELS List</div>
            <div className="rows-mt-10">
              <Select column="config_name" defaultText="Select ElasticSearch" list={esList} setValue={setSelectES} style={{ width: '450px' }} />
            </div>
            <div className="rows-mt-10">
              <Tabs
                categories={categories}
                currentCategory={currentCategory}
                setCurrentCategory={setCurrentCategory}
              />
            </div>
            <div className="rows-mt-10">
              <div className="data_mapping_area">
                <div style={currentCategory === 'DATA' ? {} : { display: 'none' }}>
                  <div className="_fr">
                    <BorderButton
                      onHandle={() => viewModal(-1)}
                      name="JSON"
                      style={{
                        width: '90px',
                        fontWeight: '200',
                        fontSize: '12px',
                        borderColor: '#aaa',
                        color: '#aaa',
                        margin: '10px'
                      }}
                    />
                  </div>
                  <DataTable columns={columns} data={resultData} openJsonModal={viewModal} />
                </div>
                <div style={currentCategory === 'Mapping' ? {} : { display: 'none' }}>
                  <div className="rows">
                    <Input name="Index" value={indexName} setValue={setIndexName} style={{ minWidth: '300px' }} />
                  </div>
                  <div className="rows">
                    <Input name="Alias" value={aliasName} setValue={setAliasName} style={{ minWidth: '300px' }} />
                  </div>
                  <div className="rows">
                    <MappingTable
                      columns={columns}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="rows">
              <div className="_fr">
                <BorderButton
                  onHandle={e => Create(e)}
                  name="SAVE"
                  style={{ width: '90px', fontWeight: '200', fontSize: '12px', borderColor: 'cornflowerblue' }}
                />
              </div>
            </div>
            <div className="h100" />
          </div>
        </div>
      </div>
      <Alert
        view={alertModal.show}
        title={alertModal.title}
        content={alertModal.content}
        hide={toggleAlert}
      />
      <Modal
        set={isModal}
        hide={toggleModal}
        title={PreWrap}
        contents={ModalContent}
        style={{ width: '1000px', height: '600px' }}
        items={{ json: resultData }}
      />
    </React.Fragment>
  );
};

export default DatabaseConfig;

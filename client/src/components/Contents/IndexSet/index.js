import React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import Section from 'components/Common/Section';
import IndexList from './IndexList';
import IndexConfig from './IndexConfig';

const Index = ({ history }) => {
  const sectionList = [
    { name: 'Index 설정 목록', url: '/index/indexlist', component: IndexList },
    { name: 'Index 설정', url: '/index/indexconfig', component: IndexConfig }
  ];

  return (
    <React.Fragment>
      <Section set="Index" sectionList={sectionList} />
      <Switch>
        {history.location.pathname === '/index' && <Redirect to={sectionList[0].url} />}
        {
          sectionList.map((item) => {
            return (
              <Route path={item.url} component={item.component} key={item.name} />
            );
          })
        }
        <Redirect from="*" exact to="/NotFound" />
      </Switch>
    </React.Fragment>
  );
};

export default withRouter(Index);

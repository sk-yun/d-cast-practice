import React, { useState } from 'react';
import BorderButton from 'common/Button/BorderButton';
import Waiting from 'common/ConnResult/Waiting';
import ConnResult from 'common/ConnResult/ConnResult';

function TableRecord(props) {
  const [record, setRecord] = useState(props.record);
  const headerSet = props.headerSet;
  const viewModal = props.viewModal;
  
  let onClick = () => {
    viewModal(props.id);
  };

  return (
    <ul onDoubleClick={() => onClick()}>
      {
        headerSet.map((item) => {
          let headerStyle = item.style || {};
          let field = item.field.replace(' ', '_');
          if (field === 'Connection') {

            let onHandle = async (e) => {
              let btn = e.target;
              // 작동중 버튼 비활성화
              btn.setAttribute('disabled', 'disabled');

              setRecord({
                ...record,
                State: <Waiting />
              });
  
              let result = await record[field]();
              let status = result ? result.status : false;
              let message = result ? result.message : 'Not receiving a response';
  
              setRecord({
                ...record,
                State: <ConnResult result={status} message={message} style={{ marginLeft: '0px' }} />
              });

              // 작동 완료 후, 버튼 활성화
              btn.removeAttribute('disabled');
            };

            return (
              <li key={field} className={field} style={headerStyle}>
                <BorderButton
                  addClass="connBtn"
                  onHandle={e => onHandle(e)}
                  name="Connection Test"
                  style={{ color: '#aaa', fontWeight: '200', fontSize: '12px' }}
                />
              </li>
            );
          }
          return (
            <li key={field} className={field} style={headerStyle}>
              {record[field]}
            </li>
          );
          
        })
      }
    </ul>
  );
}

export default TableRecord;

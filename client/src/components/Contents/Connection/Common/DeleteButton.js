import React from 'react';

const DeleteButton = (props) => {
  return (
    <button type="button" onClick={props.onHandle}>
      <span />
    </button>
  );
};

export default DeleteButton;

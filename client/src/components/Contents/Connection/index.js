import React from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import Section from 'components/Common/Section';
import DBList from './DatabaseList';
import DBConfig from './DatabaseConfig';
import ESList from './ElasticsearchList';
import ESConfig from './ElasticsearchConfig';
import NotFound from 'components/Common/NotFound';

const Connection = ({ history }) => {
  const sectionList = [
    { name: 'DB 접속 설정 목록', url: '/conn/dblist', component: DBList },
    { name: 'DB 접속 설정', url: '/conn/dbconfig', component: DBConfig },
    { name: 'ES 접속 설정 목록', url: '/conn/eslist', component: ESList },
    { name: 'ES 접속 설정', url: '/conn/esconfig', component: ESConfig }
  ];

  return (
    <React.Fragment>
      <Section set="Connection" sectionList={sectionList} />
      <Switch>
        {/* <Redirect from="/" exact to="/conn/dblist" /> */}
        {/* <Redirect from="/conn" exact to="/conn/dblist" /> */}
        {history.location.pathname === '/conn' && <Redirect to={sectionList[0].url} />}
        {
          sectionList.map((item) => {
            return (
              <Route exact path={item.url} component={item.component} key={item.name} />
            );
          })
        }
        <Redirect from="*" exact to="/NotFound" />
      </Switch>
    </React.Fragment>
  );
};

export default withRouter(Connection);

import React, { useState, useRef, useContext } from 'react';
import Axios from 'axios';
import Input from 'common/Input/Input';
import BorderButton from 'common/Button/BorderButton';
import Waiting from 'common/ConnResult/Waiting';
import ConnResult from 'common/ConnResult/ConnResult';
import Alert from 'common/Modal/ModalAlert';
import Config from 'config';
// context
import { UserInfoContext } from 'contexts/UserInfoContext';

// Axios cancel request token
const CancelToken = Axios.CancelToken;

const ElasticsearchConfig = (props) => {
  const [userInfo] = useContext(UserInfoContext);

  let paramsName = '';
  if (props.location.state) {
    paramsName = props.location.state.name;
  }

  const [name, setName] = useState(paramsName);
  const [cluster, setCluster] = useState('');
  const [node, setNode] = useState('');
  const [host, setHost] = useState('localhost');
  const [port, setPort] = useState('9200');

  const [isConnect, setConnect] = useState('');

  const errorBox = useRef(null);

  const [alertModal, setAlertModal] = useState({
    show: false,
    title: '',
    content: ''
  });

  const ConnTest = async () => {
    const source = CancelToken.source();

    errorBox.current.innerHTML = '';
    setConnect(<Waiting />);
    setCluster('');
    setNode('');

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/esConnTest`,
      method: 'post',
      data: {
        host: host,
        port: port
      }
    };
    try {
      let timeOut = setTimeout(() => {
        // cancel();
        source.cancel('Request Time Out:::30000ms');
        console.log('cancel:::', source);
        result = {
          status: false,
          message: 'Request Time Out'
        };
        return result;
      }, 30000);

      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      let response = setData.data.data.response;
      let status = response.conn_elasticsearch_connection.status;
      let cluster = response.conn_elasticsearch_connection.cluster || '';
      let node = response.conn_elasticsearch_connection.node || '';
      let error = response.conn_elasticsearch_connection.message;
      clearTimeout(timeOut);
      setConnect(<ConnResult result={status} />);
      setCluster(cluster);
      setNode(node);

      if (error) {
        let innerHTML = '<div class="errorMsg"><p>: ';
        let result = error.split(': ');
        for (let item of result) {
          if (item !== '') {
            innerHTML += item + '</br>';
          }
        }
        innerHTML += '</p></div>';
        errorBox.current.innerHTML = innerHTML;
      } else {
        errorBox.current.innerHTML = '';
      }
    } catch (e) {
      console.log('ERROR', e);
    }

  };

  // Field value validate
  const checkValidate = () => {
    // TODO: 추후 더 자세한 valition 구성하도록
    let result = false;
    let message = '';

    // Check config name
    if (!name) message = 'ES 접속 설정 이름을 설정하세요.';
    else if (name.length < 3 || name.length > 16) message = 'ES 접속 설정 이름은 4자 이상 16자 이하입니다.';
    // Ckeck other field
    else if (!host || !port) message = '비어있는 필드가 있습니다.';
    else if (!cluster || !node) message = '[Connection Test]를 진행하여 주시기바랍니다.';
    else result = true;

    // TODO: connection test 이후 등록 가능하도록 하는 로직 필요
    //        (필드 수정이 있을 시, 초기화 되도록)

    return { result, message };
  };

  const Create = async (e) => {

    let validate = checkValidate();
    
    if (validate.result) {
      if (!window.confirm('등록하시겠습니까?')) return false;

      let options = {
        url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/createESConfig`,
        method: 'post',
        data: {
          config_name: name,
          owner: userInfo.userId,
          host: host,
          port: port,
          cluster_name: cluster,
          node_name: node
        }
      };

      try {
        let setData = await Axios(options);
        if (setData.data.status === 400) {
          setAlertModal({
            show: true,
            title: '오류 메시지',
            content: setData.data.message
          });
          return false;
        }
        let response = setData.data.data.response;
        if (response.conn_create_esconfig.result === 'created') {
          props.history.push('/conn/eslist');
        }
      } catch (e) {
        console.log('ERROR', e);
      }
    } else {
      setAlertModal({
        show: true,
        title: '알림 메시지',
        content: validate.message
      });
    }
    // let newConfig = {
    //   type: 'create',
    //   config_name: name,
    //   owner: 'sun',
    //   host: host,
    //   port: port,
    //   cluster_name: cluster,
    //   node_name: node
    // };

    // let result = window.confirm('등록하시겠습니까?');
    // if (result) {
    //   const { data } = await CreateConfig(newConfig);
    //   if (data.status) {
    //     setTimeout(() => {
    //       props.history.push('/conn/eslist');
    //     }, 1000);
    //   }
    // }
  };

  const toggleAlert = () => {
    setAlertModal({
      show: false,
      title: '',
      content: ''
    });
  };


  return (
    <React.Fragment>
      <div className="ct_layout">
        <div className="ct_header">
          Elasticsearch 접속 설정
        </div>
        <div className="ct_title">
          Elasticsearch Setting
        </div>
        <div className="ct_box">
          <div className="box_conf_header">
            <Input name="Name" value={name} setValue={setName} style={{ minWidth: '600px' }} />
          </div>
          <div className="box_conf_body">
            <div className="rows">
              <Input name="Host" value={host} setValue={setHost} />
              <Input name="Port" value={port} setValue={setPort} setReg={/^[0-9\b]+$/} />
            </div>

            <div className="rows">
              <Input name="Cluster Name" value={cluster} setValue={setCluster} placeholder="Please Connection Test" disabled />
              <Input name="Node Name" value={node} setValue={setNode} placeholder="Please Connection Test" disabled />
            </div>
          </div>
          <div className="box_conf_footer">
            <div className="_fl">
              <BorderButton
                onHandle={ConnTest}
                name="Connection Test"
                style={{ color: '#aaa', fontWeight: '200', fontSize: '12px' }}
              />
              {isConnect}
            </div>

            <div className="_fr">
              <BorderButton
                onHandle={e => Create(e)}
                name="SAVE"
                style={{ width: '90px', fontWeight: '200', fontSize: '12px', borderColor: 'cornflowerblue' }}
              />
            </div>

            <div className="errorBox">
              <p ref={errorBox} />
            </div>
          </div>
        </div>
      </div>
      <Alert
        view={alertModal.show}
        title={alertModal.title}
        content={alertModal.content}
        hide={toggleAlert}
      />
    </React.Fragment>
  );
};

export default ElasticsearchConfig;

import React, { useState, useContext, useEffect, useCallback } from 'react';
import Axios from 'axios';
import sha512 from 'crypto-js/sha512';
import AddButton from 'common/Button/AddButton';
import DeleteButton from 'components/Contents/Connection/Common/DeleteButton';
import BorderButton from 'common/Button/BorderButton';
import Table from 'common/Table';
import CustomRecord from 'components/Contents/Connection/Common/CustomRecord';
import Paging from 'common/Paging';
import Alert from 'common/Modal/ModalAlert';
import Modal from 'common/Modal/ModalCover';
import ModalContent from './ConnectionModal';
import Config from 'config';

// context
import { UserInfoContext } from 'contexts/UserInfoContext';

// Axios cancel request token
const CancelToken = Axios.CancelToken;

const ElasticsearchList = ({ history }) => {
  const [userInfo] = useContext(UserInfoContext);
  const [esList, setESList] = useState([]);
  const [tableList, setTableList] = useState([]);
  const [total, setTotal] = useState(0);

  // Modal State
  const [isModal, setIsModal] = useState({
    view: false,
    data: {}
  });

  // close modal
  const toggleModal = () => {
    setIsModal({ ...isModal,
      view: !isModal.view,
      data: {}
    });
  };

  const viewModal = async (index) => {
    setIsModal({
      ...isModal,
      view: !isModal.view,
      data: esList[index]
    });
  };

  // alertModal State
  const [alertModal, setAlertModal] = useState({
    show: false,
    title: '',
    content: ''
  });

  const toggleAlert = () => {
    setAlertModal({
      show: false,
      title: '',
      content: ''
    });
  };

  const connTest = async (url) => {
    const source = CancelToken.source();

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/esConnTest`,
      method: 'post',
      data: {
        url: url
      }
    };
    try {
      let timeOut = setTimeout(() => {
        // cancel();
        source.cancel('Request Time Out:::30000ms');
        console.log('cancel:::', source);
        result = {
          status: false,
          message: 'Request Time Out'
        };
        return result;
      }, 30000);

      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      let response = setData.data.data.response;
      clearTimeout(timeOut);
      return response ? response.conn_elasticsearch_connection : result;
    } catch (e) {
      console.log('ERROR', e);
    }


  };

  const Delete = async (id) => {
    if (!window.confirm('정말로 삭제하시겠습니까?')) return false;

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/deleteESConfig`,
      method: 'post',
      data: {
        _id: id
      }
    };
    try {
      let setData = await Axios(options);
      if (setData.data.status === 400) {
        alert(setData.data.message);
        return false;
      }
      let response = setData.data.data.response;
      
      if (response.conn_elasticsearch_delete === 'deleted') {
        // window.location.reload(false);
        // props.history.push('/conn/eslist');
        // eslint-disable-next-line no-use-before-define
        // getCofigList(0);
        // return <Refresh path="/refresh" />;
      }
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  // List 전체 Connection Test
  const batchConnTest = useCallback(() => {
    // react에 적용하기엔 좋지 못하다고 판단되는 코드
    let btn = document.getElementsByClassName('connBtn');
    let i = 0;
    const delayBatch = (i) => {
      if (i < btn.length) {
        btn[i].click();
        setTimeout(() => delayBatch(++i), 100);
      }
    };
    delayBatch(i);
  }, [esList]);

  const checkConnTest = () => {
    let btn = document.getElementsByClassName('connBtn');
    let shouldBlock = false;
    for (let i = 0; i < btn.length; i++) {
      if (btn[i].getAttribute('disabled') === 'disabled') {
        shouldBlock = true;
        return shouldBlock;
      }
    }
    return shouldBlock;
  };

  const blockAlert = () => {
    let content = (
      <p>
        [Connection Test] 중에는 페이지를 이동할 수 없습니다.
        <br />
        [Connection Test]를 [Cancel] 하거나 동작이 완료되기를 기다려주십시오.
      </p>
    );
    setAlertModal({
      show: true,
      title: '차단 메시지',
      content: content
    });
  };

  const onBlock = () => {
    if (checkConnTest()) {
      blockAlert();
      return false;
    }
    return true;
  };

  // Get ES Config List
  const getCofigList = useCallback(async (count) => {
    if (checkConnTest()) {
      blockAlert();
      return false;
    }
    setESList([]);
    setTableList([]);


    // 새로고침이나 경로를 통해서 접속 시, userId 정보가 사라진다.
    // app.js에서 해당 정보를 session에서 가져오기 전에 호출되어
    // owner가 빈 값이 되는 문제가 있는데, 이를 해결하기 위한 코드.
    let owner = userInfo.userId;
    if (!owner || owner === '') {
      owner = window.sessionStorage.getItem(sha512('id'));
    }

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/esConfigList`,
      method: 'post',
      data: {
        owner: owner,
        from: count,
        size: 5
      }
    };
    try {
      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      
      let response = setData.data.data.response;
      let dataSet = response.conn_esconfig_data;
      let total = response.conn_esconfig_total;
      setTotal(total);
      setESList(dataSet);

      let result = [];
      for (let i = 0; i < dataSet.length; i++) {
        const id = dataSet[i]._id;
        const url = dataSet[i].host + ':' + dataSet[i].port;
        // const ConnectionBtn = (
        //   <BorderButton
        //     onHandle={e => onHandle(e)}
        //     name="Connection Test"
        //     style={{ color: '#aaa', fontWeight: '200', fontSize: '12px' }}
        //   />
        // );

        const ConnTest = () => {
          return connTest(url);
        };

        const DeleteBtn = (
          <DeleteButton onHandle={() => Delete(id)} />
        );

        let item = {
          Session: dataSet[i].config_name,
          // Node_Name: dataSet[i].node_name,
          Url: url,
          Cluster_Name: dataSet[i].cluster_name,
          Create_Date: dataSet[i].create_date,
          Connection: ConnTest,
          State: 'Waiting',
          Delete: DeleteBtn
        };

        result.push(item);
      }
      setTableList(result);

      // batchConnTest();
    } catch (e) {
      console.log('ERROR', e);
    }
  }, []);

  // 초기 DB List 받아오기.
  useEffect(() => {
    getCofigList(0);
  }, []);

  // page 전환 block 함수
  useEffect(() => {
    const unblock = history.block(() => {
      if (checkConnTest()) {
        blockAlert();
        return false;
      }
    });
    return () => {
      unblock();
    };
  }, [history]);

  const headerSet = [
    { field: 'Session', type: 'text' },
    { field: 'Url', type: 'text' },
    // { field: 'Node Name', type: 'text' },
    { field: 'Cluster Name', type: 'text' },
    { field: 'Create Date', type: 'text' },
    { field: 'Connection', type: 'function' },
    { field: 'State', type: 'text' },
    { field: 'Delete', type: 'function', style: { minWidth: '50px' } },
  ];

  return (
    <React.Fragment>
      <div className="ct_layout">
        <div className="ct_header">
          Elasticsearch 접속 설정 목록
        </div>
        <div className="ct_title">
          Elasticsearch List
        </div>
        <div className="ct_box">
          <AddButton set="Connection" url="/conn/esconfig" text="Add Session" />
          <Table
            headerSet={headerSet}
            data={tableList}
            viewModal={viewModal}
            CustomRecord={CustomRecord}
            tableStyle={{ minWidth: '1140px' }}
          />
          <div className="ct_box_footer" style={{ width: '1138px' }}>
            <Paging
              onClick={getCofigList}
              block={onBlock}
              totalCount={total}
              listCount={5}
              displayCount={5}
              current={1}
            />
            <BorderButton
              onHandle={batchConnTest}
              name="Batch Connection Test"
              style={{
                width: '165px',
                color: '#aaa',
                fontWeight: '200',
                fontSize: '12px',
                float: 'right',
                margin: '10px'
              }}
            />
          </div>
        </div>
      </div>
      <Alert
        view={alertModal.show}
        title={alertModal.title}
        content={alertModal.content}
        hide={toggleAlert}
      />
      <Modal
        set={isModal}
        hide={toggleModal}
        contents={ModalContent}
        style={{ width: '550px', height: '465px' }}
        items={{ setAlertModal: setAlertModal }}
      />
    </React.Fragment>
  );
};

export default ElasticsearchList;

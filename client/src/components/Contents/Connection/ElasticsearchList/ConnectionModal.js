import React, { useState, useContext, useRef } from 'react';
import Axios from 'axios';
import Input from 'common/Input/Input';
import Waiting from 'common/ConnResult/Waiting';
import ConnResult from 'common/ConnResult/ConnResult';
import BorderButton from 'common/Button/BorderButton';
import Config from 'config';
import 'common/Modal/Modal.scss';

// context
import { UserInfoContext } from 'contexts/UserInfoContext';

// Axios cancel request token
const CancelToken = Axios.CancelToken;

const ModalContents = (props) => {
  const [userInfo] = useContext(UserInfoContext);
  const [state, setState] = useState({});
  const [isConnect, setConnect] = useState('');
  const [flag, setFlag] = useState(false); // connection check용 state

  const errorBox = useRef(null);
  const hide = props.hide;
  const setAlertModal = props.items.setAlertModal;

  const inintialize = () => {
    let size = 0;
    
    if (Object.keys) {
      // IE8 이하는 지원하지 않음.
      size = Object.keys(state).length;
    } else {
      for (let key in state) {
        if (state.hasOwnProperty.call(state, key)) size++;
      }
    }

    // state값이 존재하지 않을 경우
    if (!size > 0) setState(props.data);
    // state값이 존재하나 기존 값과 다른 경우 state 교체
    else if (size > 0 && state._id !== props.data._id) setState(props.data);
    else return false;

    
    // connection result 초기화
    setConnect('');
  };

  // Connection Test
  const ConnTest = async () => {
    const source = CancelToken.source();

    errorBox.current.innerHTML = '';
    setConnect(<Waiting />);

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/esConnTest`,
      method: 'post',
      data: {
        host: state.host,
        port: state.port
      }
    };
    try {
      let timeOut = setTimeout(() => {
        // cancel();
        source.cancel('Request Time Out:::30000ms');
        console.log('cancel:::', source);
        result = {
          status: false,
          message: 'Request Time Out'
        };
        return result;
      }, 30000);

      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      let response = setData.data.data.response;
      let status = response.conn_elasticsearch_connection.status;
      let cluster = response.conn_elasticsearch_connection.cluster || '';
      let node = response.conn_elasticsearch_connection.node || '';
      let error = response.conn_elasticsearch_connection.message;
      console.log(cluster, node);
      clearTimeout(timeOut);
      setConnect(<ConnResult result={status} />);
      setState({
        ...state,
        cluster_name: cluster,
        node_name: node
      });

      if (error) {
        let innerHTML = '<div class="errorMsg"><p>: ';
        let result = error.split(': ');
        for (let item of result) {
          if (item !== '') {
            innerHTML += item + '</br>';
          }
        }
        innerHTML += '</p></div>';
        errorBox.current.innerHTML = innerHTML;
      } else {
        errorBox.current.innerHTML = '';
      }
    } catch (e) {
      console.log('ERROR', e);
    }

  };

  // Field value validate
  const checkValidate = () => {
    // TODO: 추후 더 자세한 validate 구성하도록
    let result = false;
    let message = '';

    // Check config name
    // if (!name) message = 'ES 접속 설정 이름을 설정하세요.';
    // else if (name.length < 3 || name.length > 16) message = 'ES 접속 설정 이름은 4자 이상 16자 이하입니다.';
    // Ckeck other field
    if (!state.host || !state.port) message = '비어있는 필드가 있습니다.';
    else if (!state.cluster_name || !state.node_name) message = '[Connection Test]를 진행하여 주시기바랍니다.';
    else result = true;

    // TODO: connection test 이후 등록 가능하도록 하는 로직 필요
    //        (필드 수정이 있을 시, 초기화 되도록)

    return { result, message };
  };

  const Save = async () => {
    console.log('asd');
    console.log(state);
    let validate = checkValidate();
    
    if (validate.result) {
      if (!window.confirm('수정하시겠습니까?')) return false;

      let options = {
        url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/updateESConfig`,
        method: 'post',
        data: {
          id: state._id,
          config_name: state.name,
          // owner: userInfo.userId,
          host: state.host,
          port: state.port,
          cluster_name: state.cluster,
          node_name: state.node
        }
      };

      try {
        let setData = await Axios(options);
        if (setData.data.status === 400) {
          setAlertModal({
            show: true,
            title: '오류 메시지',
            content: setData.data.message
          });
          return false;
        }
        let response = setData.data.data.response;
        if (response.conn_update_esconfig.result === 'updated') {
          hide();
          // TODO: 수정된 리스트로 다시 리로드 해올 방법
          window.location.reload(); // 임시방편
          // props.history.push('/conn/eslist');
        }
      } catch (e) {
        console.log('ERROR', e);
      }
    } else {
      setAlertModal({
        show: true,
        title: '알림 메시지',
        content: validate.message
      });
    }
    
  };

  inintialize();

  return (
    <React.Fragment>
      <div className="modal_content _conn">
        <div className="box_div">
          <div className="box_layout noshadow">
            <div className="_conn">
              <div className="desc_div">
                <div className="desc_left">
                  <div className="conn_title">{state.config_name}</div>
                </div>
              </div>
            </div>

            <div className="_content fx_h_300">
              <div className="grid_box">
                <div className="rows">
                  <Input
                    name="Node Name"
                    value={state.node_name}
                    setValue={nodeName => setState({
                      ...state,
                      node_name: nodeName
                    })}
                    disabled
                  />
                </div>
                <div className="rows">
                  <Input
                    name="Cluster Name"
                    value={state.cluster_name}
                    setValue={clusterName => setState({
                      ...state,
                      cluster_name: clusterName
                    })}
                    disabled
                  />
                </div>
                <div className="rows">
                  <Input
                    name="Host"
                    value={state.host}
                    setValue={host => setState({
                      ...state,
                      host: host
                    })}
                  />
                </div>
                <div className="rows">
                  <Input
                    name="Port"
                    value={state.port}
                    setReg={/^[0-9\b]+$/}
                    setValue={port => setState({
                      ...state,
                      port: port
                    })}
                  />
                </div>
                <div className="rows _connBtn">
                  <BorderButton
                    onHandle={ConnTest}
                    name="Connection Test"
                    style={{ color: '#aaa', fontWeight: '200', fontSize: '12px' }}
                  />
                  {isConnect}
                  <div className="errorBox" ref={errorBox} />
                </div>
                
                <div className="rows _saveBtn">
                  <BorderButton
                    onHandle={e => Save(e)}
                    name="SAVE"
                    style={{
                      width: '120px',
                      fontWeight: '200',
                      fontSize: '12px',
                      borderColor: 'cornflowerblue',
                      color: 'white',
                      backgroundColor: 'cornflowerblue'
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default ModalContents;

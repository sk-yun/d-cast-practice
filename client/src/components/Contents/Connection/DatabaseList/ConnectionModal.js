import React, { useState, useContext, useRef } from 'react';
import Axios from 'axios';
import Input from 'common/Input/Input';
import Waiting from 'common/ConnResult/Waiting';
import ConnResult from 'common/ConnResult/ConnResult';
import BorderButton from 'common/Button/BorderButton';
import Config from 'config';
import 'common/Modal/Modal.scss';

// context
import { UserInfoContext } from 'contexts/UserInfoContext';

// Axios cancel request token
const CancelToken = Axios.CancelToken;

const ModalContents = (props) => {
  const [userInfo] = useContext(UserInfoContext);
  const [state, setState] = useState({});
  const [isConnect, setConnect] = useState('');

  const errorBox = useRef(null);
  const hide = props.hide;
  const setAlertModal = props.items.setAlertModal;

  const inintialize = () => {
    let size = 0;
    
    if (Object.keys) {
      // IE8 이하는 지원하지 않음.
      size = Object.keys(state).length;
    } else {
      for (let key in state) {
        if (state.hasOwnProperty.call(state, key)) size++;
      }
    }

    // state값이 존재하지 않을 경우
    if (!size > 0) setState(props.data);
    // state값이 존재하나 기존 값과 다른 경우 state 교체
    else if (size > 0 && state._id !== props.data._id) setState(props.data);
    else return false;

    // connection result 초기화
    setConnect('');
  };

  // Connection Test
  const ConnTest = async () => {
    const source = CancelToken.source();
    setConnect(<Waiting />);
    errorBox.current.innerHTML = '';

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/dbConnTest`,
      method: 'post',
      data: {
        drivername: state.drivername,
        url: state.url,
        user: state.user,
        password: state.password
      }
    };
    try {
      let result = null;
      let timeOut = setTimeout(() => {
        // cancel();
        source.cancel('Request Time Out:::30000ms');
        console.log('cancel:::', source);
        result = {
          status: false,
          message: 'Request Time Out'
        };
        return result;
      }, 30000);

      let setData = await Axios(options);
      if (setData.data.status === 400) {
        alert(setData.data.message);
        return false;
      }
      let response = setData.data.data.response;
      let status = response.conn_database_connection.status;
      let error = response.conn_database_connection.message;
      clearTimeout(timeOut);
      setConnect(<ConnResult result={status} />);

      if (error) {
        let innerHTML = '<div class="errorMsg"><p>: ';
        let result = error.split(': ');
        for (let item of result) {
          if (item !== '') {
            innerHTML += item + '</br>';
          }
        }
        innerHTML += '</p></div>';
        errorBox.current.innerHTML = innerHTML;
      } else {
        errorBox.current.innerHTML = '';
      }
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  // Field value validate
  const checkValidate = () => {
    // TODO: 추후 더 자세한 valition 구성하도록
    let result = false;
    let message = '';

    // Ckeck other field
    if (!state.host || !state.port || !state.database || !state.user || !state.password || !state.url) message = '비어있는 입력 필드가 있습니다.';
    else result = true;

    // TODO: connection test 이후 등록 가능하도록 하는 로직 필요
    //        (필드 수정이 있을 시, 초기화 되도록)

    return { result, message };
  };

  const Save = async () => {
    console.log('Save:::');

    let validate = checkValidate();
    
    if (validate.result) {
      if (!window.confirm('수정하시겠습니까?')) return false;
      // config_name 수정가능 여부는 나중에 생각.
      let options = {
        url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/updateDBConfig`,
        method: 'post',
        data: {
          id: state._id,
          config_name: state.config_name,
          host: state.host,
          port: state.port,
          database: state.database,
          user: state.user,
          password: state.password,
          url: state.url
        }
      };

      try {
        let setData = await Axios(options);
        if (setData.data.status === 400) {
          setAlertModal({
            show: true,
            title: '오류 메시지',
            content: setData.data.message
          });
          return false;
        }
        let response = setData.data.data.response;
        if (response.conn_update_dbconfig.result === 'updated') {
          hide();
          // TODO: 수정된 리스트로 다시 리로드 해올 방법
          window.location.reload(); // 임시방편
          // props.history.push('/conn/dblist');
        }
      } catch (e) {
        console.log('ERROR', e);
      }
    } else {
      setAlertModal({
        show: true,
        title: '알림 메시지',
        content: validate.message
      });
    }
  };

  inintialize();

  return (
    <React.Fragment>
      <div className="modal_content _conn">
        <div className="box_div">
          <div className="box_layout noshadow">
            <div className="_conn">
              <div className="desc_div">
                <div className="desc_left">
                  <div className="conn_title">{state.config_name}</div>
                </div>
              </div>
            </div>

            <div className="_content fx_h_380">
              <div className="grid_box">
                <div className="rows">
                  {/* <Select
                    name="DB Vendor"
                    default={vendor._id}
                    list={vendorList}
                    setValue={setVendorInfo}
                  /> */}
                  <Input name="DB Vendor" value={state.vendor} style={{ width: '150px' }} disabled />
                </div>
                <div className="rows">
                  <Input
                    name="Host"
                    value={state.host}
                    setValue={host => setState({
                      ...state,
                      host: host
                    })}
                  />
                </div>
                <div className="rows">
                  <Input
                    name="Port"
                    value={state.port}
                    setReg={/^[0-9\b]+$/}
                    setValue={port => setState({
                      ...state,
                      port: port
                    })}
                  />
                </div>
                <div className="rows">
                  <Input
                    name="Database(SID)"
                    value={state.database}
                    setValue={database => setState({
                      ...state,
                      database: database
                    })}
                  />
                </div>
                <div className="rows">
                  <Input
                    name="User"
                    value={state.user}
                    setValue={user => setState({
                      ...state,
                      user: user
                    })}
                    style={{ width: '90px', marginRight: '20px' }}
                  />
                  <Input
                    type="password"
                    name="Password"
                    value={state.password}
                    setValue={password => setState({
                      ...state,
                      password: password
                    })}
                    style={{ width: '90px' }}
                  />
                </div>
                <div className="rows">
                  <Input
                    name="URL"
                    value={state.url}
                    setValue={url => setState({
                      ...state,
                      url: url
                    })}
                  />
                </div>
                <div className="rows _connBtn">
                  <BorderButton
                    onHandle={ConnTest}
                    name="Connection Test"
                    style={{ color: '#aaa', fontWeight: '200', fontSize: '12px' }}
                  />
                  {isConnect}
                  <div className="errorBox" ref={errorBox} />
                </div>
                
                <div className="rows _saveBtn">
                  <BorderButton
                    onHandle={e => Save(e)}
                    name="SAVE"
                    style={{
                      width: '120px',
                      fontWeight: '200',
                      fontSize: '12px',
                      borderColor: 'cornflowerblue',
                      color: 'white',
                      backgroundColor: 'cornflowerblue'
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default ModalContents;

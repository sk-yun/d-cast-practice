import React, { useState, useContext, useEffect, useRef } from 'react';
import Axios from 'axios';
import Input from 'common/Input/Input';
import Select from 'common/Select/Select';
import BorderButton from 'common/Button/BorderButton';
import Waiting from 'common/ConnResult/Waiting';
import ConnResult from 'common/ConnResult/ConnResult';
import Alert from 'common/Modal/ModalAlert';
import Config from 'config';
// context
import { UserInfoContext } from 'contexts/UserInfoContext';

// Axios cancel request token
const CancelToken = Axios.CancelToken;

const DatabaseConfig = (props) => {
  const [userInfo] = useContext(UserInfoContext);

  let paramsName = '';
  if (props.location.state) {
    paramsName = props.location.state.name || '';
  }

  const [name, setName] = useState(paramsName);
  const [vendorList, setVendorList] = useState([]);
  const [vendor, setVendor] = useState({});
  const [host, setHost] = useState('localhost');
  const [port, setPort] = useState('');
  const [database, setDatabase] = useState('');
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');
  const [url, setUrl] = useState('');
  const [isConnect, setConnect] = useState('');

  const errorBox = useRef(null);

  const [alertModal, setAlertModal] = useState({
    show: false,
    title: '',
    content: ''
  });

  // Get VendorList
  const getVendorList = async () => {
    // 계정별로 vendor를 관리할지 어떨지 고민해야할 듯.
    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/vendorList`,
      method: 'post',
      data: { userId: userInfo.userId }
    };
    try {
      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      let response = setData.data.data.response;
      setVendorList(response.conn_vendor_data);
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  // Connection Test
  const ConnTest = async () => {
    const source = CancelToken.source();
    setConnect(<Waiting />);
    errorBox.current.innerHTML = '';

    let options = {
      url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/dbConnTest`,
      method: 'post',
      data: {
        drivername: vendor.drivername,
        url: url,
        user: user,
        password: password
      }
    };
    try {
      let result = null;
      let timeOut = setTimeout(() => {
        // cancel();
        source.cancel('Request Time Out:::30000ms');
        console.log('cancel:::', source);
        result = {
          status: false,
          message: 'Request Time Out'
        };
        return result;
      }, 30000);

      let setData = await Axios(options);
      if (setData.data.status === 400) {
        setAlertModal({
          show: true,
          title: '오류 메시지',
          content: setData.data.message
        });
        return false;
      }
      let response = setData.data.data.response;
      let status = response.conn_database_connection.status;
      let error = response.conn_database_connection.message;
      clearTimeout(timeOut);
      setConnect(<ConnResult result={status} />);

      if (error) {
        let innerHTML = '<div class="errorMsg"><p>: ';
        let result = error.split(': ');
        for (let item of result) {
          if (item !== '') {
            innerHTML += item + '</br>';
          }
        }
        innerHTML += '</p></div>';
        errorBox.current.innerHTML = innerHTML;
      } else {
        errorBox.current.innerHTML = '';
      }
    } catch (e) {
      console.log('ERROR', e);
    }
  };

  // Field value validate
  const checkValidate = () => {
    // TODO: 추후 더 자세한 valition 구성하도록
    let result = false;
    let message = '';

    // Check config name
    if (!name) message = 'DB 접속 설정 이름을 설정하세요.';
    else if (name.length < 3 || name.length > 16) message = 'DB 접속 설정 이름은 4자 이상 16자 이하입니다.';
    // Ckeck other field
    else if (!host || !port || !database || !user || !password || !url) message = '비어있는 입력 필드가 있습니다.';
    else result = true;

    // TODO: connection test 이후 등록 가능하도록 하는 로직 필요
    //        (필드 수정이 있을 시, 초기화 되도록)

    return { result, message };
  };
  
  // Create DB Config
  const Create = async () => {
    console.log('Create:::');

    let validate = checkValidate();
    
    if (validate.result) {
      if (!window.confirm('등록하시겠습니까?')) return false;

      let options = {
        url: `http://${Config.API_HOST.IP}:${Config.API_HOST.PORT}/api/conn/createDBConfig`,
        method: 'post',
        data: {
          config_name: name,
          owner: userInfo.userId,
          vendor: vendor.vendor,
          host: host,
          port: port,
          database: database,
          drivername: vendor.drivername,
          user: user,
          password: password,
          url: url
        }
      };

      try {
        let setData = await Axios(options);
        if (setData.data.status === 400) {
          setAlertModal({
            show: true,
            title: '오류 메시지',
            content: setData.data.message
          });
          return false;
        }
        let response = setData.data.data.response;
        if (response.conn_create_dbconfig.result === 'created') {
          props.history.push('/conn/dblist');
        }
      } catch (e) {
        console.log('ERROR', e);
      }
    } else {
      setAlertModal({
        show: true,
        title: '알림 메시지',
        content: validate.message
      });
    }

  };

  // vendor 리스트 출력
  useState(() => {
    getVendorList();
  }, [vendorList]);

  // url 자동생성
  useEffect(() => {
    if (vendor.url_template !== undefined) {
      let url = vendor.url_template;

      url = url.replace('{HOST}', host)
        .replace('{PORT}', port)
        .replace('{DATABASE}', database);

      setUrl(url);
    }
  }, [vendor, host, port, database]);

  // vendor 선택 시 기본 port 추가
  useEffect(() => {
    if (Object.keys(vendor).length !== 0) {
      setPort(vendor.port);
    }
  }, [vendor]);

  const toggleAlert = () => {
    setAlertModal({
      show: false,
      title: '',
      content: ''
    });
  };

  return (
    <React.Fragment>
      <div className="ct_layout">
        <div className="ct_header">
          DB 접속 설정
        </div>
        <div className="ct_title">
          DB Setting
        </div>
        <div className="ct_box">
          <div className="box_conf_header">
            <Input name="Name" value={name} setValue={setName} style={{ minWidth: '600px' }} />
          </div>
          <div className="box_conf_body">
            <div className="rows">
              <Select name="DB Vendor" column="vendor" defaultText="Select Vendor" list={vendorList} setValue={setVendor} />
            </div>

            <div className="rows">
              <Input name="Host" value={host} setValue={setHost} style={{ marginRight: '9vw' }} />
              <Input name="Port" value={port} setValue={setPort} setReg={/^[0-9\b]+$/} />
            </div>

            <div className="rows">
              <Input name="Database(SID)" setValue={setDatabase} />
            </div>

            <div className="rows">
              <Input name="User" value={user} setValue={setUser} style={{ marginRight: '9vw' }} />
              <Input type="password" name="Password" value={password} setValue={setPassword} />
            </div>

            <div className="rows">
              <Input name="URL" value={url} setValue={setUrl} />
            </div>
          </div>
          <div className="box_conf_footer">
            <div className="_fl">
              <BorderButton
                onHandle={ConnTest}
                name="Connection Test"
                style={{ color: '#aaa', fontWeight: '200', fontSize: '12px' }}
              />
              {isConnect}
              <div className="errorBox" ref={errorBox} />
            </div>

            <div className="_fr">
              <BorderButton
                onHandle={e => Create(e)}
                name="SAVE"
                style={{ width: '90px', fontWeight: '200', fontSize: '12px', borderColor: 'cornflowerblue' }}
              />
            </div>
            
          </div>
        </div>
      </div>
      <Alert
        view={alertModal.show}
        title={alertModal.title}
        content={alertModal.content}
        hide={toggleAlert}
      />
    </React.Fragment>
  );
};

export default DatabaseConfig;

import React from 'react';
import './ConnResult.css';

function ConnResult(props) {

  const isSuccess = props.result;
  const style = props.style || {};
  const message = props.message;

  return (
    <React.Fragment>
      {
        isSuccess
          ? <div className="Success" style={style}>Success</div>
          : (
            <div className="Fail" style={style}>
              Fail
              {message && <span className="message" title={message} /> }
            </div>
          )
      }
    </React.Fragment>
  );
}
export default ConnResult;

import React from 'react';
import './Waiting.css';

function Waiting() {

  return (
    <div className="waiting">
      <div className="lds-ring">
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
}

export default Waiting;

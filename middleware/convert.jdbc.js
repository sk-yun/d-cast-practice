const _ = require('lodash');
const Statement = require('jdbc/lib/statement');
const ResultSet = require('jdbc/lib/resultset');
const elasticsearch = require('./es.client.service');

/*
  해당 모듈은 jdbc 모듈 버전 0.6.4/ 0.7.0 에서만 테스트 되었음.
    =>  npm install jdbc@^0.7.0
*/

let info = {};

// 시간 출력을 위한 time foramt 함수
function msToTime(duration) {
  let seconds = parseInt((duration / 1000) % 60, 10);
  let minutes = parseInt((duration / (1000 * 60)) % 60, 10);
  let hours = parseInt((duration / (1000 * 60 * 60)) % 24, 10);

  hours = (hours < 10) ? '0' + hours : hours;
  minutes = (minutes < 10) ? '0' + minutes : minutes;
  seconds = (seconds < 10) ? '0' + seconds : seconds;

  return hours + ':' + minutes + ':' + seconds;
}

// convert Statement
Statement.prototype.executeBulkQuery = function (option, callback) {
  let sql = option.query;
  if (typeof sql === 'string') {
    this._s.executeQuery(sql, (err, resultset) => {
      if (err) {
        return callback(err);
      }
      info = option;
      return callback(null, new ResultSet(resultset));
    });
  } else {
    return callback(new Error('INVALID ARGUMENTS'));
  }
};


// convert ResultSet
ResultSet.prototype.tobulkArray = function (callback) {
  this.toBulkObject((err, result) => {
    if (err) return callback(err);
    callback(null, result.rows);
  });
};
  
ResultSet.prototype.toBulkObject = function (callback) {
  this.toObjectIter(async (err, rs) => {
    if (err) return callback(err);
    
    let rowIter = rs.rows;
    let rows = [];
    let row = rowIter.next();
  
    const bulksize = info.bulksize;
    const host = info.host;
    const index = info.index;

    let payload;
  
    try {
      await elasticsearch.initialize(host);
      console.log('[::: Start Bulk Insert :::]');
      let startTime = new Date().getTime();
      let num = 0;
  
      while (!row.done) {
        num++;
        row.value.aaa = num;
        payload = {};
        payload.index = {
          _index: index.toLowerCase(),
          _type: '_doc'
        };
  
        rows.push(payload);
        rows.push(row.value);
  
        row = rowIter.next();

        if (num % bulksize === 0) {
          // console.log(rows);
          let indexingStartTime = new Date().getTime();
          await elasticsearch.addBulk(rows);
          let indexingEndTime = new Date().getTime();
          console.log('[Bulk insert] :', num + '건, [Indexed Time : ',
            (indexingEndTime - indexingStartTime), 'ms] - Total Time :',
            msToTime(indexingEndTime - startTime));

          rows = [];
        } else if (row.done) {
          // console.log(rows);
          await elasticsearch.addBulk(rows);
          let endTime = new Date().getTime();
          console.log('[::: Bulk insert complete :::] :', num + '건, [Total Working Time :',
            msToTime(endTime - startTime), ']');
          rows = [];
        }
      }
      rs.rows = rows;
    } catch (err2) {
      console.log(err2);
    }
  
    return callback(null, rs);
  });
};

const JDBC = require('jdbc');
require('./convert.jdbc');

const Pool = function () {
  this.Jdbc = null;
  this.Conn = null;
  this.Statement = null;
  
  this.result = {};

  this.setUp = (config) => {
    if (this.Jdbc === null) {
      this.Jdbc = new JDBC(config);
    }
  };

  this.setDown = () => {
    this.Jdbc = null;
    this.Conn = null;
    this.Statement = null;
  };

  this.initialize = () => {
    return new Promise((resolve, reject) => {
      this.Jdbc.initialize((err) => {
        if (err) {
          const errorList = err.message.split('\n');
          let error = errorList[1];
          reject(error);
        } else {
          this.Jdbc.reserve((err, connObj) => {
            if (connObj) {
              this.result.client = this.Jdbc;
              this.result.connObj = connObj;
              this.Conn = connObj.conn;
              resolve({
                status: true,
                result: this.result
              });
            } else {
              reject(err);
            }
          });
        }
      });
    });
  };

  this.setAutoCommit = (isAutoCommit) => {
    return new Promise((resolve, reject) => {
      this.Conn.setAutoCommit(isAutoCommit, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve({ status: true });
        }
      });
    });
  };

  this.createStatement = () => {
    return new Promise((resolve, reject) => {
      this.Conn.createStatement((err, statement) => {
        if (err) {
          reject(err);
        } else {
          this.Statement = statement;
          resolve({ status: true });
        }
      });
    });
  };

  this.convertDataType = (results) => {
    // decimal과 같은 객체로 넘어오는 데이터 처리를 위한 로직
    for (let i in results) {
      if (Object.prototype.hasOwnProperty.call(results, i)) {
        for (let column in results[i]) {
          if (Object.prototype.hasOwnProperty.call(results[i], column)) {
            if (typeof results[i][column] === 'object') {
              results[i][column] = '' + results[i][column];
            }
          }
        }
      }
    }
    return results;
  };

  this.executeQuery = (query) => {
    return new Promise((resolve, reject) => {
      this.Statement.executeQuery(query, (err, resultset) => {
        if (err) {
          reject(err);
        } else {
          resultset.toObjArray((err, results) => {
            if (err) {
              reject(err);
            } else {
              // decimal과 같은 객체로 넘어오는 데이터 처리를 위한 로직
              if (resultset._types.indexOf('BigDecimal') > -1) {
                results = this.convertDataType(results);
              }

              resolve({
                status: true,
                result: results
              });
            }
          });
        }
      });
    });
  };

  this.setFetchSize = (count) => {
    return new Promise((resolve, reject) => {
      this.Statement.setFetchSize(count, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  this.setMaxRows = (count) => {
    return new Promise((resolve, reject) => {
      this.Statement.setMaxRows(count, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  };

  this.closeStatement = () => {
    return new Promise((resolve, reject) => {
      if (this.Statement !== null) {
        this.Statement.close((err) => {
          if (err) {
            reject(err);
          } else {
            this.Statement = null;
            resolve();
          }
        });
      }
    });
  };

  this.close = () => {
    return new Promise((resolve, reject) => {
      if (this.Statement !== null) {
        this.Statement.close((err) => {
          if (err) {
            reject(err);
          } else {
            this.Statement = null;
          }
        });
      }
      this.Conn.close((err) => {
        if (err) {
          reject(err);
        }
      });
      this.Jdbc.release(this.Conn, (err) => {
        if (err) {
          reject(err);
        } else {
          this.Conn = null;
          this.Jdbc = null;
          resolve();
        }
      });
    });
  };

  this.bulk = (esHost, indexName, query, bulkSize) => {
    return new Promise((resolve, reject) => {
      // validate
      if (!esHost || !indexName || !query) {
        throw new Error('Bulk Insert invalid arguments');
      } else if (typeof esHost !== 'string' || typeof indexName !== 'string'
        || typeof query !== 'string' || (bulkSize && typeof bulkSize !== 'number')) {
        throw new Error('The type of arguments is invalid');
      }
  
      if (bulkSize < 1 || bulkSize > 10000) {
        throw new Error('\'bulkSize\' is between 1 and 10000, inclusive.');
      }
  
      let option = {
        host: esHost,
        index: indexName,
        query: query,
        bulksize: bulkSize || 1000
      };
  
      this.Statement.executeBulkQuery(option, (err, resultset) => {
        if (err) {
          reject(err);
        } else {
          resultset.tobulkArray((err, results) => {
            if (err) {
              reject(err);
            } else {
              resolve(results);
            }
          });
        }
      });
    });
  };
};

module.exports = Pool;

// let Pool = null;
// let Conn = null;
// let Statement = null;

// let result = {};

// const setUp = (config) => {
//   if (Pool === null) {
//     Pool = new JDBC(config);
//   }
// };

// const setDown = () => {
//   Pool = null;
//   Conn = null;
//   Statement = null;
// };

// const initialize = () => {
//   return new Promise((resolve, reject) => {
//     Pool.initialize((err) => {
//       if (err) {
//         const errorList = err.message.split('\n');
//         let error = errorList[1];
//         reject(error);
//       } else {
//         Pool.reserve((err, connObj) => {
//           if (connObj) {
//             result.client = Pool;
//             result.connObj = connObj;
//             Conn = connObj.conn;
//             resolve({
//               status: true,
//               result: result
//             });
//           } else {
//             reject(err);
//           }
//         });
//       }
//     });
//   });
// };

// const setAutoCommit = (isAutoCommit) => {
//   return new Promise((resolve, reject) => {
//     Conn.setAutoCommit(isAutoCommit, (err) => {
//       if (err) {
//         reject(err);
//       } else {
//         resolve({ status: true });
//       }
//     });
//   });
// };

// const createStatement = () => {
//   return new Promise((resolve, reject) => {
//     Conn.createStatement((err, statement) => {
//       if (err) {
//         reject(err);
//       } else {
//         Statement = statement;
//         resolve({ status: true });
//       }
//     });
//   });
// };

// const executeQuery = (query) => {
//   return new Promise((resolve, reject) => {
//     Statement.executeQuery(query, (err, resultset) => {
//       if (err) {
//         reject(err);
//       } else {
//         resultset.toObjArray((err, results) => {
//           if (err) {
//             reject(err);
//           } else {
//             resolve({
//               status: true,
//               result: results
//             });
//           }
//         });
//       }
//     });
//   });
// };

// const setFetchSize = (count) => {
//   return new Promise((resolve, reject) => {
//     Statement.setFetchSize(count, (err) => {
//       if (err) {
//         reject(err);
//       } else {
//         resolve();
//       }
//     });
//   });
// };

// const setMaxRows = (count) => {
//   return new Promise((resolve, reject) => {
//     Statement.setMaxRows(count, (err) => {
//       if (err) {
//         reject(err);
//       } else {
//         resolve();
//       }
//     });
//   });
// };

// const closeStatement = () => {
//   return new Promise((resolve, reject) => {
//     if (Statement !== null) {
//       Statement.close((err) => {
//         if (err) {
//           reject(err);
//         } else {
//           Statement = null;
//           resolve();
//         }
//       });
//     }
//   });
// };

// const close = () => {
//   return new Promise((resolve, reject) => {
//     if (Statement !== null) {
//       Statement.close((err) => {
//         if (err) {
//           reject(err);
//         } else {
//           Statement = null;
//         }
//       });
//     }
//     Conn.close((err) => {
//       if (err) {
//         reject(err);
//       }
//     });
//     Pool.release(Conn, (err) => {
//       if (err) {
//         reject(err);
//       } else {
//         Conn = null;
//         Pool = null;
//         resolve();
//       }
//     });
//   });
// };

// const bulk = (esHost, indexName, query, bulkSize) => {
//   return new Promise((resolve, reject) => {
//     // validate
//     if (!esHost || !indexName || !query) {
//       throw new Error('Bulk Insert invalid arguments');
//     } else if (typeof esHost !== 'string' || typeof indexName !== 'string'
//       || typeof query !== 'string' || (bulkSize && typeof bulkSize !== 'number')) {
//       throw new Error('The type of arguments is invalid');
//     }

//     if (bulkSize < 1 || bulkSize > 10000) {
//       throw new Error('\'bulkSize\' is between 1 and 10000, inclusive.');
//     }

//     let option = {
//       host: esHost,
//       index: indexName,
//       query: query,
//       bulksize: bulkSize || 1000
//     };

//     Statement.executeBulkQuery(option, (err, resultset) => {
//       if (err) {
//         reject(err);
//       } else {
//         resultset.tobulkArray((err, results) => {
//           if (err) {
//             reject(err);
//           } else {
//             resolve(results);
//           }
//         });
//       }
//     });
//   });
// };

// module.exports = {
//   setUp: setUp,
//   setDown: setDown,
//   initialize: initialize,
//   setAutoCommit: setAutoCommit,
//   createStatement: createStatement,
//   executeQuery: executeQuery,
//   setFetchSize: setFetchSize,
//   setMaxRows: setMaxRows,
//   closeStatement: closeStatement,
//   close: close,
//   bulk: bulk
// };

const approot = require('app-root-path');
const jinst = require('jdbc/lib/jinst');
const fs = require('fs');

module.exports = () => {
  let classpath = [];
  
  // drivers 등록
  let filelist = fs.readdirSync(approot + '/drivers', (err) => {
    if (err) console.log(err);
  });
        
  if (!filelist || filelist.length === 0) {
    console.error('Driver not found');
    return false;
  }
              
  filelist.forEach((item) => {
    classpath.push(approot + '/drivers/' + item);
  });
          
  if (!jinst.isJvmCreated()) {
    jinst.addOption('-Xrs');
    jinst.setupClasspath(classpath);
  }
};

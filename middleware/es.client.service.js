const elasticsearch = require('elasticsearch');

let elasticClient;

module.exports = {
  // 특정 ES에 대한 Connection test
  initialize: async (elasticUrl) => {
    let result = {};
    elasticClient = await new elasticsearch.Client({
      host: elasticUrl
    });

    await elasticClient
      .ping({
        requestTimeout: 3000
      })
      .then((r) => {
        result.status = true;
        result.client = elasticClient;
      })
      .catch((error) => {
        result.status = false;
        result.error = error;
        // throw error;
      });

    return result;
  },

  // create index
  createIndex: async (indexName, mapping) => elasticClient.indices.create({
    index: indexName
  }).catch((error) => {
    console.log(error);
    throw error;
  }),
  /* bulk Inserting */
  addBulk: async payload => elasticClient.bulk({
    refresh: 'wait_for',
    body: payload
  }).catch((err) => {
    console.log(err);
    throw err;
  })
};

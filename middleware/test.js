const setUpClasspath = require('./java.classpath');
const DataBase = require('./jdbc.service');

setUpClasspath();

// oracle
let config = {
  drivername: 'oracle.jdbc.OracleDriver',
  url: 'jdbc:oracle:thin:@127.0.0.1:1521:xe',
  user: 'kh',
  password: 'kh',
  minpoolsize: 1,
  maxpoolsize: 3
};

// mariaDB => 현재 mariadb만 안되는 중
let config2 = {
  drivername: 'org.mariadb.jdbc.Driver',
  url: 'jdbc:mariadb://192.168.0.120:3306/test',
  user: 'root',
  password: '1234',
  minpoolsize: 1,
  maxpoolsize: 3
};

// mysql
let config3 = {
  drivername: 'com.mysql.cj.jdbc.Driver',
  url: 'jdbc:mysql://54.180.122.93:3306/ubuntu_db',
  user: 'ubuntu',
  password: 'ubuntu',
  minpoolsize: 1,
  maxpoolsize: 3
};

// postgresql
let config4 = {
  drivername: 'org.postgresql.Driver',
  url: 'jdbc:postgresql://54.180.122.93:5432/young',
  user: 'young',
  password: 'young',
  minpoolsize: 1,
  maxpoolsize: 3
};

// altibase - kisa
let config5 = {
  drivername: 'Altibase6_5.jdbc.driver.AltibaseDriver',
  url: 'jdbc:Altibase://222.122.202.182:20300/CTIS',
  user: 'ctisdev',
  password: 'rhfemsroqkf1811',
  minpoolsize: 1,
  maxpoolsize: 3
};

// mysql
let configTest = {
  drivername: 'com.mysql.cj.jdbc.Driver',
  url: 'jdbc:mysql://192.168.0.126:3306/music_DB',
  user: 'root',
  password: 'hmsk6534',
  minpoolsize: 1,
  maxpoolsize: 3
};

// docker mysql
let config6 = {
  drivername: 'com.mysql.cj.jdbc.Driver',
  url: 'jdbc:mysql://localhost:3306/test_db?serverTimezone=Asia/Seoul&useUnicode=true&characterEncoding=utf8',
  user: 'skyun',
  password: 'asd123456',
  minpoolsize: 1,
  maxpoolsize: 3
};


const test = async () => {
  try {
    let Pool = new DataBase();
    Pool.setUp(config6);
    let data = await Pool.initialize();
    data = await Pool.createStatement();
    // let query = 'SELECT * FROM test'; //config 2
    // let query = 'SELECT * FROM myTable'; // config3
    // let query = 'SELECT * FROM kdata_rss_scrapdata'; // config3
    // let query = 'SELECT * FROM mytable'; // config4
    // let query = 'SELECT * FROM DF_CTIS_TB_2019_TERM_CLASS'; // config5
    let query = 'select * from products';
    // let query = 'show tables';
    // await Pool.setFetchSize(100);
    // await Pool.setMaxRows(10);
    // data = await Pool.bulk('127.0.0.1:9200', 'tempdata', query, 10000);
    data = await Pool.executeQuery(query);
    console.log(data);

    await Pool.close();
  } catch (err) {
    console.log(err);
  }
};
  
test();
